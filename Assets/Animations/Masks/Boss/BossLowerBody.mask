%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: BossLowerBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: lutenit
    m_Weight: 1
  - m_Path: lutenit/Arm_L
    m_Weight: 1
  - m_Path: lutenit/Arm_L/elbow_L
    m_Weight: 1
  - m_Path: lutenit/Arm_L/Hand_L
    m_Weight: 1
  - m_Path: lutenit/Arm_L/upper_am_L
    m_Weight: 1
  - m_Path: lutenit/Arm_L/Wrist_L
    m_Weight: 1
  - m_Path: lutenit/Arm_R
    m_Weight: 1
  - m_Path: lutenit/Arm_R/elbow_R
    m_Weight: 1
  - m_Path: lutenit/Arm_R/hand_R
    m_Weight: 1
  - m_Path: lutenit/Arm_R/Upper_am_R
    m_Weight: 1
  - m_Path: lutenit/Arm_R/Wrist_R
    m_Weight: 1
  - m_Path: lutenit/cockpit
    m_Weight: 1
  - m_Path: lutenit/Leg_L
    m_Weight: 1
  - m_Path: lutenit/Leg_L/foot_L
    m_Weight: 1
  - m_Path: lutenit/Leg_L/hip_L
    m_Weight: 1
  - m_Path: lutenit/Leg_L/leg_L
    m_Weight: 1
  - m_Path: lutenit/Leg_R
    m_Weight: 1
  - m_Path: lutenit/Leg_R/foot_R
    m_Weight: 1
  - m_Path: lutenit/Leg_R/hip_R
    m_Weight: 1
  - m_Path: lutenit/Leg_R/leg_R
    m_Weight: 1
  - m_Path: lutenit/wiast
    m_Weight: 1
  - m_Path: root_jnt
    m_Weight: 1
  - m_Path: root_jnt/Hip_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_L_JNT/Knee_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_L_JNT/Knee_L_JNT/Ankle_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_L_JNT/Knee_L_JNT/Ankle_L_JNT/Toe_L_JNT_END
    m_Weight: 1
  - m_Path: root_jnt/Hip_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_R_JNT/Knee_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_R_JNT/Knee_R_JNT/Ankle_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/Hip_R_JNT/Knee_R_JNT/Ankle_R_JNT/Toe_R_JNT_END
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_L_JNT/Elbow_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_L_JNT/Elbow_L_JNT/wrist_L_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_L_JNT/Elbow_L_JNT/wrist_L_JNT/hand_L_JNT_END
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_R_JNT/Elbow_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_R_JNT/Elbow_R_JNT/wrist_R_JNT
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/shoulder_R_JNT/Elbow_R_JNT/wrist_R_JNT/hand_R_JNT_END
    m_Weight: 1
  - m_Path: root_jnt/torso_hip_jnt/Shoulders_JNT/torso_JNT_END
    m_Weight: 1
  - m_Path: torso_root_jnt_CTRL_GRP
    m_Weight: 1
  - m_Path: torso_root_jnt_CTRL_GRP/torso_root_jnt_CTRL
    m_Weight: 1
  - m_Path: torso_root_jnt_CTRL_GRP/torso_root_jnt_CTRL/Torso_hip_jnt_CTRL_GRP
    m_Weight: 1
  - m_Path: torso_root_jnt_CTRL_GRP/torso_root_jnt_CTRL/Torso_hip_jnt_CTRL_GRP/Hip_jnt_CTRL
    m_Weight: 1
