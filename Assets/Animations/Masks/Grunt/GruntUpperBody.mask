%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: GruntUpperBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ctrl_route
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_left_leg
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_left_leg/ctrl_left_upper_no_leg
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_left_leg/ctrl_left_upper_no_leg/leg_left_knee
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_left_leg/ctrl_left_upper_no_leg/leg_left_knee/leg_left_ankle
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_left_leg/ctrl_left_upper_no_leg/leg_left_knee/leg_left_ankle/leg_left_foot
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_right_leg
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_right_leg/leg_right_upper_no_edit
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_right_leg/leg_right_upper_no_edit/leg_right_knee
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_right_leg/leg_right_upper_no_edit/leg_right_knee/leg_right_ankle
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_spine_mid/ctrl_spine_insert/ctrl_hip/ctrl_fmr_right_leg/leg_right_upper_no_edit/leg_right_knee/leg_right_ankle/leg_right_foot
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_upper
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_upper/ctrl_eye_outer
    m_Weight: 0
  - m_Path: ctrl_route/ctrl_upper/ctrl_left_clav
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_left_clav/ctrl_left_sldr
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_left_clav/ctrl_left_sldr/ctrl_left_claw
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_left_clav/ctrl_left_sldr/ctrl_left_claw/ctrl_left_claw_piston
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_left_clav/ctrl_left_sldr/ctrl_left_claw/ctrl_left_claw_piston/ctrl_left_claw_manip
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_right_clav
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_right_clav/ctrl_right_sldr
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_right_clav/ctrl_right_sldr/ctrl_right_gun
    m_Weight: 1
  - m_Path: ctrl_route/ctrl_upper/ctrl_right_clav/ctrl_right_sldr/ctrl_right_gun/ctrl_right_gun_end_noedit
    m_Weight: 1
  - m_Path: ctrl_route_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_eye_outer_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_eye_outer_CTRL_GRP/ctrl_eye_outer_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP/ctrl_left_sldr_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP/ctrl_left_sldr_CTRL/ctrl_left_claw_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP/ctrl_left_sldr_CTRL/ctrl_left_claw_CTRL_GRP/ctrl_left_claw_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP/ctrl_left_sldr_CTRL/ctrl_left_claw_CTRL_GRP/ctrl_left_claw_CTRL/ctrl_left_claw_manip_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_left_clav_CTRL_GRP/ctrl_left_clav_CTRL/ctrl_left_sldr_CTRL_GRP/ctrl_left_sldr_CTRL/ctrl_left_claw_CTRL_GRP/ctrl_left_claw_CTRL/ctrl_left_claw_manip_CTRL_GRP/ctrl_left_claw_manip_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP/ctrl_right_clav_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP/ctrl_right_clav_CTRL/ctrl_right_sldr_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP/ctrl_right_clav_CTRL/ctrl_right_sldr_CTRL_GRP/ctrl_right_sldr_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP/ctrl_right_clav_CTRL/ctrl_right_sldr_CTRL_GRP/ctrl_right_sldr_CTRL/ctrl_right_gun_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_right_clav_CTRL_GRP/ctrl_right_clav_CTRL/ctrl_right_sldr_CTRL_GRP/ctrl_right_sldr_CTRL/ctrl_right_gun_CTRL_GRP/ctrl_right_gun_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP/ctrl_fmr_left_leg_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP/ctrl_fmr_left_leg_CTRL/leg_left_knee_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP/ctrl_fmr_left_leg_CTRL/leg_left_knee_CTRL_GRP/leg_left_knee_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP/ctrl_fmr_left_leg_CTRL/leg_left_knee_CTRL_GRP/leg_left_knee_CTRL/leg_left_ankle_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_left_leg_CTRL_GRP/ctrl_fmr_left_leg_CTRL/leg_left_knee_CTRL_GRP/leg_left_knee_CTRL/leg_left_ankle_CTRL_GRP/leg_left_ankle_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP/ctrl_fmr_right_leg_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP/ctrl_fmr_right_leg_CTRL/leg_right_knee_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP/ctrl_fmr_right_leg_CTRL/leg_right_knee_CTRL_GRP/leg_right_knee_CTRL
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP/ctrl_fmr_right_leg_CTRL/leg_right_knee_CTRL_GRP/leg_right_knee_CTRL/leg_right_ankle_CTRL_GRP
    m_Weight: 0
  - m_Path: ctrl_route_CTRL_GRP/ctrl_route_CTRL/ctrl_spine_mid_CTRL_GRP/ctrl_spine_mid_CTRL/ctrl_spine_insert_CTRL_GRP/ctrl_spine_insert_CTRL/ctrl_hip_CTRL_GRP/ctrl_hip_CTRL/ctrl_fmr_right_leg_CTRL_GRP/ctrl_fmr_right_leg_CTRL/leg_right_knee_CTRL_GRP/leg_right_knee_CTRL/leg_right_ankle_CTRL_GRP/leg_right_ankle_CTRL
    m_Weight: 0
  - m_Path: grunt_mdl_lower
    m_Weight: 1
  - m_Path: grunt_mdl_torso
    m_Weight: 1
