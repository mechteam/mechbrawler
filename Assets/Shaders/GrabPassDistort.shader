﻿Shader "Unlit/GrabPassDistort"
{
	Properties
	{
		_ZBufferNudge("ZBufferNudge", Float) = 0.01
	}
	SubShader
	{
		// Draw ourselves after all opaque geometry and particles (queue = 3001)
		Tags { "Queue" = "Transparent+2" }

		// Grab the screen behind the object into _BackgroundTexture
		GrabPass
		{
			"_BackgroundTexture2"
		}

		// Render the object with the texture generated above, and invert the colors
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float3 uv : TEXCOORD0;
				float4 grabPos : TEXCOORD1;
				float4 pos : SV_POSITION;
			};

			struct appdata
			{
				float4 vertex : POSITION;
				float3 uv : TEXCOORD0;
			};

			float _ZBufferNudge;

			v2f vert(appdata v) {
				v2f o;
				// use UnityObjectToClipPos from UnityCG.cginc to calculate 
				// the clip-space of the vertex
				o.pos = UnityObjectToClipPos(v.vertex);
				// use ComputeGrabScreenPos function from UnityCG.cginc
				// to get the correct texture coordinate
				o.grabPos = ComputeGrabScreenPos(o.pos);
				// pass base tex coords through
				o.uv = v.uv;
				o.pos.z += _ZBufferNudge;
				return o;
			}

			sampler2D _BackgroundTexture2;

			float4 distort(float4 uvScreen, float3 uvMesh)
			{
				// get mesh coords in -1 to 1 range
				float2 uv0 = (2 * uvMesh) - 1;
				// get distance from centre of quad
				float dist = length(uv0); // range 0 at centre to 1 at edge

				float power = 1 + 100*(0.25 - (uvMesh.z-0.5) * (uvMesh.z-0.5));
				float distMod = pow(dist, 1/power);

				float attenuation = saturate(1 - 10* saturate(dist - 0.9));
				//float2 delta = float2(uv0.x, uv0.y);
				float2 delta = uv0 * (distMod-dist);
				delta *= attenuation;
				clip(1-dist);
				return uvScreen + float4(delta, 0 , 0);
			}


			half4 frag(v2f i) : SV_Target
			{
				half4 bgcolor = tex2Dproj(_BackgroundTexture2, distort(i.grabPos, i.uv));

				i.pos.z = 0.0f;
				return bgcolor;
			}
			ENDCG
		}

	}
	Fallback "Diffuse"
}
