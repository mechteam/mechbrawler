﻿using UnityEngine;

public class FaceVelocity : MonoBehaviour
{
    private Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(rb.velocity.normalized != Vector3.zero)
            transform.forward = rb.velocity.normalized;
    }
}
