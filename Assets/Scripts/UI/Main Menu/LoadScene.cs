﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private void Awake()
    {
        CarbonVanguard.CharInput.LockMouse(false);
    }

    public void ChangeToScene(int sceneNumber)
    {
        SceneManager.LoadScene(sceneNumber, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
