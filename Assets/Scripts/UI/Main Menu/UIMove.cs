﻿using UnityEngine;
public class UIMove : MonoBehaviour
{
    public Vector3 targetLocation;
    public float speed;
    public float delay;

    private void Start()
    {
        Pixelplacement.Tween.Position(transform, targetLocation, speed, delay, Pixelplacement.Tween.EaseInStrong);
    }

}
