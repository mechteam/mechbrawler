﻿using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    public class ImageFade : MonoBehaviour
    {
        public enum FadeType
        {
            IN,
            OUT
        }

        public FadeType fadeType;
        public float fadeSpeed;
        public float delay;
        private Image image;
        private Text text;
        private float ticker;

        void Awake()
        {
            image = GetComponent<Image>();
            if (image == null)
            {
                text = GetComponent<Text>();
            }
            if (image == null && text == null)
            {
                Debug.Log("No object to fade could be found", this);
            }
            ticker = 0f;
        }

        void Update()
        {
            if (ticker < delay)
            {
                ticker += Time.deltaTime;
            }
            if (ticker >= delay)
            {
                if (fadeType == FadeType.IN)
                {
                    if (image != null)
                    {
                        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + fadeSpeed * Time.deltaTime);
                    }
                    else if (text != null)
                    {
                        text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a + fadeSpeed * Time.deltaTime);
                    }
                    else
                    {
                        Debug.Log("No object to fade in", this);
                    }
                }
                else if (fadeType == FadeType.OUT)
                {
                    if (image != null)
                    {
                        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - fadeSpeed * Time.deltaTime);
                    }
                    else if (text != null)
                    {
                        text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - fadeSpeed * Time.deltaTime);
                    }
                    else
                    {
                        Debug.Log("No object to fade in", this);
                    }
                }
            }
        }
    }
}