﻿using UnityEngine;

public class SizeUp : MonoBehaviour
{
    public Vector3 newSize = new Vector3(1, 1, 1);

    private Vector3 regularSize;

    private void Awake()
    {
        regularSize = transform.localScale;
    }

    public void OnEnter()
    {
        Pixelplacement.Tween.LocalScale(transform, newSize, 0.25f, 0, Pixelplacement.Tween.EaseBounce);
    }

    public void OnExit()
    {
        Pixelplacement.Tween.LocalScale(transform, regularSize, 0.25f, 0, Pixelplacement.Tween.EaseBounce);
    }
}
