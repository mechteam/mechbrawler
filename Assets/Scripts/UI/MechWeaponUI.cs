﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace CarbonVanguard
{
    public class MechWeaponUI : MonoBehaviour
    {
        public Mech player;
        public CharInput input;

        [Header("Strings")]
        public string heavyWeaponReady = "READY";
        public string heavyWeaponCooldown = "WAIT";
        public string reloadingText = "RELOADING";
        public string emptyWeapon = "EMPTY";

        [Header("Crosshair")]
        public Image rightCrosshairImg;
        public Image leftCrosshairImg;

        [Header("Ammo UI")]
        public TextMeshProUGUI leftWeaponLabel;
        public TextMeshProUGUI rightWeaponLabel;
        public Image leftAmmoBar;
        public Image rightAmmoBar;

        [Header("UtilityU")]
        public Image[] fuelBars;

        public TextMeshProUGUI heavyWeaponText;
        public TextMeshProUGUI leftWeaponText;
        public TextMeshProUGUI rightWeaponText;

        private void UpdateWeaponStatus()
        {
            string lArmDisp = player.leftArm.equipment.data != null ? player.leftArm.equipment.data.uiName : emptyWeapon;

            string hWepStatus = player.torso.equipment.data != null ? (player.torso.equipment.Ready ? heavyWeaponReady : heavyWeaponCooldown) : string.Empty;

            string hWepDisp = player.torso.equipment.data != null ? string.Format("{0}\n{1}", player.torso.equipment.data.uiName, hWepStatus) : emptyWeapon;

            string rArmDisp = player.rightArm.equipment.data != null ? player.rightArm.equipment.data.uiName : emptyWeapon;

            string lAmmoDisp = player.leftArm.equipment.data != null ? (!player.leftArm.equipment.reloading ? player.leftArm.equipment.resource.ToString() : reloadingText) : string.Empty;
            string rAmmoDisp = player.rightArm.equipment.data != null ? (!player.rightArm.equipment.reloading ? player.rightArm.equipment.resource.ToString() : reloadingText) : string.Empty;

            if (leftAmmoBar != null && player.leftArm.equipment.data != null)
            {
                leftAmmoBar.enabled = true;
                leftAmmoBar.fillAmount = player.leftArm.equipment.resource.Percentage;
            }
            else
                leftAmmoBar.enabled = false;
                

            if (leftWeaponLabel != null)
                leftWeaponLabel.text = lArmDisp;

            if (rightAmmoBar != null && player.leftArm.equipment.data != null)
            {
                rightAmmoBar.enabled = true;
                rightAmmoBar.fillAmount = player.rightArm.equipment.resource.Percentage;
            }
            else
                rightAmmoBar.enabled = false;

            if (rightWeaponLabel != null)
                rightWeaponLabel.text = rArmDisp;

            if (heavyWeaponText != null)
                heavyWeaponText.text = hWepDisp;

            if (leftWeaponText != null)
                leftWeaponText.text = lAmmoDisp;

            if (rightWeaponText != null)
                rightWeaponText.text = rAmmoDisp;

        }

        private void UpdateCrosshair()
        {
            Weapon left = null, right = null, heavy = null;

            if (player.leftArm.equipment != null)
                left = player.leftArm.equipment.data as Weapon;

            if (player.rightArm.equipment != null)
                right = player.leftArm.equipment.data as Weapon;

            if (player.torso.equipment != null)
                heavy = player.leftArm.equipment.data as Weapon;

            if (input != null)
            {
                if (CharCmd.heavyWeapon && heavy != null)
                {
                    leftWeaponLabel.enabled = false;
                    leftWeaponText.enabled = false;

                    rightWeaponText.enabled = false;
                    rightWeaponLabel.enabled = false;

                    heavyWeaponText.enabled = false;

                    leftAmmoBar.enabled = false;
                    rightAmmoBar.enabled = false;

                    heavyWeaponText.enabled = true;


                    if (rightCrosshairImg != null)
                        rightCrosshairImg.sprite = heavy.weaponCrosshair;

                    if (leftCrosshairImg != null)
                        leftCrosshairImg.enabled = false;
                }
                else
                {
                    leftWeaponLabel.enabled = true;
                    leftWeaponText.enabled = true;

                    rightWeaponText.enabled = true;
                    rightWeaponLabel.enabled = true;

                    leftAmmoBar.enabled = true;
                    rightAmmoBar.enabled = true;

                    heavyWeaponText.enabled = false;

                    if (right != null)
                    {
                        if (rightCrosshairImg != null)
                            rightCrosshairImg.sprite = right.weaponCrosshair;
                    }

                    if (left != null)
                    {
                        if (leftCrosshairImg != null)
                        {
                            leftCrosshairImg.enabled = true;
                            leftCrosshairImg.sprite = left.weaponCrosshair;
                        }
                            
                    }
                }
            }
        }

        private void UpdateFuel()
        {
            if (player.rightLeg.equipment != null)
            {
                foreach (Image bar in fuelBars)
                {
                    bar.fillAmount = player.rightLeg.equipment.resource.Percentage;
                }
            }
        }

        private void Update()
        {
            if (player != null)
            {
                UpdateWeaponStatus();
                UpdateCrosshair();
                UpdateFuel();
            }
        }
    }
}