﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    [RequireComponent(typeof(Button))]
    public class EquipmentSelectButton : MonoBehaviour
    {
        private Customiser customiser;
        private int equipmentIndex;

        public void Initialise(Customiser customiser, int index)
        {
            this.customiser = customiser;
            equipmentIndex = index;

            Text buttonLabel = GetComponentInChildren<Text>();

            if (buttonLabel != null)
                buttonLabel.text = customiser.selectedEqipmentList[index].uiName;

            Button button = GetComponent<Button>();

            button.onClick.AddListener(delegate { customiser.SelectEquipment(index); });
        }
    }
}