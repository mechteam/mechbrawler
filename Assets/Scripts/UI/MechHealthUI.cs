﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    public class MechHealthUI : MonoBehaviour
    {
        public Mech target;

        public Gradient healthGradient;
        public Gradient shieldGradient;
        public List<Image> partHealthImages;
        public Image shieldImage;

        private void Start()
        {
            if (target != null)
            { 
                target.energyShield.OnValueChanged += delegate { UIUtils.UpdateImage(shieldImage, target.energyShield, shieldGradient); };
                
                    
                for (int i = 0; i < target.GetParts().Count; i++)
                {
                    if (i >= partHealthImages.Count)
                        break;

                    if (partHealthImages[i] == null || target.GetParts()[i] == null)
                        continue;

                    target.GetParts()[i].health.OnValueChanged += delegate { UIUtils.UpdateImage(partHealthImages[i], target.GetParts()[i].health, healthGradient); };
                    
                }
            }
        }

        private void Update()
        {
            ForceUpdateHUD();
        }

        private void ForceUpdateHUD()
        {
            UIUtils.UpdateImage(shieldImage, target.energyShield, shieldGradient);

            for (int i = 0; i < target.GetParts().Count; i++)
            {
                if (i >= partHealthImages.Count)
                    break;

                if (partHealthImages[i] == null || target.GetParts()[i] == null)
                    continue;

                UIUtils.UpdateImage(partHealthImages[i], target.GetParts()[i].health, healthGradient);
            }
        }
    }
}