﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    public class PartSelectButton : MonoBehaviour
    {
        private Customiser customiser;
        private int partIndex;

        public void Initialise(Customiser customiser, int index)
        {
            this.customiser = customiser;
            partIndex = index;

            Button button = GetComponent<Button>();
            button.onClick.AddListener(OnClicked);

            Text buttonLabel = GetComponentInChildren<Text>();

            if(buttonLabel != null)
                buttonLabel.text = customiser.target.GetParts()[index].uiName;
        }

        private void OnClicked()
        {
            customiser.SelectPart(partIndex);
        }
    }
}