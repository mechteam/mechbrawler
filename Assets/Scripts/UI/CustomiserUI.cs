﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public class CustomiserUI : MonoBehaviour
    {
        public Customiser customiser;
        public PartSelectButton partButtonPrefab;
        public EquipmentSelectButton equipmentButtonPrefab;
        public RectTransform partMenuParent;
        public RectTransform equipmentMenuParent;

        private List<PartSelectButton> partMenu;
        private List<EquipmentSelectButton> equipmentMenu;

        private void Awake()
        {
            customiser.MechPartSelected += CreateEquipmentMenu;
            customiser.MechEquipmentSet += DestroyEquipmentMenu;
        }

        private void Start()
        {
            equipmentMenu = new List<EquipmentSelectButton>(0);
            partMenu = new List<PartSelectButton>(0);
            CreatePartMenu();
        }

        public void CreatePartMenu()
        {
            DestroyPartMenu();

            for (int i = 0; i < customiser.target.GetParts().Count; i++)
            {
                if (customiser.target.GetParts()[i].excludeFromCustomiser)
                    continue;

                PartSelectButton button = Instantiate<PartSelectButton>(partButtonPrefab, partMenuParent != null ? partMenuParent : transform);
                button.Initialise(customiser, i);

                partMenu.Add(button);
            }
        }

        public void CreateEquipmentMenu()
        {
            DestroyEquipmentMenu();

            for (int i = 0; i < customiser.selectedEqipmentList.Length; i++)
            {
                EquipmentSelectButton button = Instantiate<EquipmentSelectButton>(equipmentButtonPrefab, equipmentMenuParent != null ? equipmentMenuParent : transform);
                button.Initialise(customiser, i);

                equipmentMenu.Add(button);
            }
        }

        public void DestroyPartMenu()
        {
            for (int i = 0; i < partMenu.Count; i++)
            {
                Destroy(partMenu[i].gameObject);
            }

            partMenu.Clear();
        }

        public void DestroyEquipmentMenu()
        {
            for (int i = 0; i < equipmentMenu.Count; i++)
            {
                Destroy(equipmentMenu[i].gameObject);
            }

            equipmentMenu.Clear();
        }
    }
}