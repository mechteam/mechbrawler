﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace CarbonVanguard
{
    public class EquipmentSlotUI : MonoBehaviour, IResettable
    {
        public EquipmentSlot target;

        public Image resourceBar;
        public TextMeshProUGUI titleText;
        public TextMeshProUGUI resourceText;

        public TextMeshProUGUI lResourceText;
        public TextMeshProUGUI rResourceText;

        public void Reset()
        {
            if (resourceBar != null)
            {
                 resourceBar.fillAmount = target.resource.Percentage;
            }

            if (resourceText != null)
            {
                resourceText.text = target.resource.ToString();
            }

            if (titleText != null && target.data != null)
            {
                titleText.text = target.data.uiName;
            }
        }

        private void Update()
        {
            if (target != null)
            {
                if (resourceBar != null)
                {
                    resourceBar.fillAmount = target.resource.Percentage;
                }

                if (resourceText != null)
                {
                    resourceText.text = target.resource.ToString();
                }

                if (titleText != null && target.data != null)
                {
                    titleText.text = target.data.uiName;
                }

                if (lResourceText != null)
                {
                    lResourceText.text = target.resource.ToString();
                }

                if (rResourceText != null)
                {
                    rResourceText.text = target.resource.ToString();
                }
            }

            Reset();
        }
    }
}