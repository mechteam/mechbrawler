﻿//#define DEBUG_PLAYER_INPUT
using UnityEngine;

namespace CarbonVanguard
{
    [RequireComponent(typeof(CharacterController))]
    public class CharInput : MonoBehaviour
    {
        public static CharInput Instance { get; private set; }
        public Mech playerMech;
        public Camera playerView;

        private CharacterController controller;

        [Header("Input Settings")]
        public CharCmd cmd;
        private CVInput input;

        [Header("Movement Speed Settings")]
        public float jumpSpeed;
        public float acceleration;
        public float gravity;
        public float friction;
        public float groundCheckTolerance;
        public LayerMask groundCheckMask;
        public static bool isGrounded { get; private set; }

        private Vector3 velocity;
#if DEBUG_PLAYER_INPUT
        private GameObject debugContactObj;
#endif

        [Header("Mouse Look")]
        [Tooltip("When enabled, use the vector2 to control look sensitivity on each axis separately")]
        public bool useSeparateAxisSens;
        public bool invertMouse;
        public float sensitivity;
        public Vector2 vectorSens;

        public float defaultFOV;
        public float zoomFOV;

        [Range(-90, 0)]
        public float minLookClamp = -70;
        [Range(0, 90)]
        public float maxLookClamp = 90;

        //Start rotation to base mouseLook on
        private Quaternion originalRotation;

        //Values grabbed from the hardware
        private float mouseX;
        private float mouseY;

        //Quaternions used to apply rotation
        private Quaternion rotX;
        private Quaternion rotY;

        public static void LockMouse(bool value)
        {
            Cursor.visible = !value;
            Cursor.lockState =  value ? CursorLockMode.Locked : CursorLockMode.None;
        }

        private void MouseLook()
        {
            float multX = useSeparateAxisSens ? vectorSens.x : sensitivity;
            float multY = useSeparateAxisSens ? vectorSens.y : sensitivity;

            mouseX += CharCmd.lookInput.x * multX * Time.deltaTime;
            mouseY += (invertMouse ? CharCmd.lookInput.y : -CharCmd.lookInput.y) * multX * Time.deltaTime;

            mouseY = Mathf.Clamp(mouseY, minLookClamp, maxLookClamp);

            rotX = Quaternion.AngleAxis(mouseX, Vector3.up);
            rotY = Quaternion.AngleAxis(mouseY, Vector3.right);

            transform.localRotation = rotX * originalRotation;
            playerView.transform.localRotation = rotY * originalRotation;
        }

        private void GroundCheck()
        {
            float dist = (controller.height / 2) + groundCheckTolerance;
            float width = controller.radius + controller.skinWidth;
            Vector3 pos = transform.position + controller.center;

            if (Physics.SphereCast(pos, width,-transform.up, out RaycastHit hit, dist, groundCheckMask, QueryTriggerInteraction.Ignore))
            {
#if DEBUG_PLAYER_INPUT
                debugContactObj = hit.collider.gameObject;
#endif
                isGrounded = true;
            }
            else
                isGrounded = false;

#if DEBUG_PLAYER_INPUT
            Debug.DrawLine(pos, pos + (-transform.up * dist), Color.red, 0.1f);
#endif
        }

        public static void Impulse(Vector3 direction)
        {
            Instance.velocity += Instance.transform.TransformDirection(direction);
        }

        private void Awake()
        {
            input = new CVInput();

            if (Instance == null)
                Instance = this;
            else
                Destroy(this);

            controller = GetComponent<CharacterController>();

            if (playerView == null)
            {
                playerView = Camera.main;
                Debug.LogWarning("No camera specified. Using Camera.main");
            }

            cmd = new CharCmd();
        }

        private void Start()
        {
            originalRotation = transform.localRotation;
            input.Player.SetCallbacks(cmd);
        }

        private void OnEnable()
        {
            if (input != null)
                input.Player.Enable();

            LockMouse(true);
        }

        private void OnDisable()
        {
            if (input != null)
                input.Player.Disable();

            LockMouse(false);
        }

        private void Accelerate(float speed)
        {
            Vector3 addVel = new Vector3(CharCmd.moveInput.x, 0, CharCmd.moveInput.y);
            velocity += transform.TransformDirection((addVel * speed) * Time.deltaTime);
        }

        private void ApplyFriction(float f)
        {
            Vector3 decel = (velocity * f) * Time.deltaTime;
            velocity -= decel;
        }

        private void Update()
        {
            if (playerMech == null)
                return;

#region Movement
            GroundCheck();
            MouseLook();

            velocity.y = controller.velocity.y;

            if (playerMech.rightLeg.equipment != null)
            {
                if (CharCmd.utilityItem)
                {
                    playerMech.rightLeg.equipment.Use();
                }
                else
                {
                    playerMech.rightLeg.equipment.usingUtil = false;
                }
            }

            if (!isGrounded)
            {
                velocity.y -= gravity * Time.deltaTime;
            }
            else
            {
                Accelerate(acceleration);
                ApplyFriction(friction);
            }

            controller.Move(velocity * Time.deltaTime);
#endregion

#region Mech Equipment

            if (!CharCmd.heavyWeapon)
            {
                if (CharCmd.leftWeapon && playerMech.leftArm.equipment != null && !playerMech.leftArm.equipment.reloading)
                    playerMech.leftArm.Use();

                if (CharCmd.rightWeapon && playerMech.rightArm.equipment != null && !playerMech.rightArm.equipment.reloading)
                    playerMech.rightArm.Use();

                if (!CharCmd.leftWeapon && playerMech.leftArm.equipment != null && !playerMech.leftArm.equipment.reloading)
                    playerMech.leftArm.Stop();

                if (!CharCmd.rightWeapon && playerMech.rightArm.equipment != null && !playerMech.rightArm.equipment.reloading)
                    playerMech.rightArm.Stop();
            }
            else
            {
                playerMech.rightArm.Stop();
                playerMech.leftArm.Stop();

                if ((CharCmd.leftWeapon || CharCmd.rightWeapon) && playerMech.torso.equipment != null)
                    playerMech.torso.Use();
            }

            if (CharCmd.utilityItem && playerMech.rightLeg.equipment != null)
                playerMech.rightLeg.Use();

            if (CharCmd.reloadLeft || CharCmd.reloadBoth && !playerMech.leftArm.equipment.reloading)
                playerMech.leftArm.equipment.ManualReload();

            if (CharCmd.reloadRight || CharCmd.reloadBoth && !playerMech.rightArm.equipment.reloading)
                playerMech.rightArm.equipment.ManualReload();

            if (CharCmd.zoomToggle)
                playerView.fieldOfView = Mathf.Lerp(playerView.fieldOfView, zoomFOV, 0.5f);
            else
                playerView.fieldOfView = Mathf.Lerp(playerView.fieldOfView, defaultFOV, 0.5f);
            #endregion
        }

#if DEBUG_PLAYER_INPUT
        public override string ToString()
        {
            return string.Format("Input: {0}\nVelocity: {1}\nGrounded: {2}\nContact: {3}\n{4}", acceleration, controller.velocity, isGrounded, debugContactObj != null ? debugContactObj.name : "nothing", cmd.ToString());
        }

        private void OnGUI()
        {
            GUI.Box(new Rect(Screen.width - 256, 16, 250, 300), ToString());
        }
#endif
    }
}