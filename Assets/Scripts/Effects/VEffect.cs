﻿using UnityEngine;

namespace CarbonVanguard
{
    public class VEffect : MonoBehaviour, IObjectPooled
    {
        public SoundSelector sfxSelector;
        public bool useVfx;

        private ParticleSystem vfxSource;
        private AudioSource sfxSource;

        [SerializeField]
        private string _poolName;

        public string poolName
        {
            get
            {
                return _poolName;
            }
            set
            {
                _poolName = value;
            }
        }

        private void Awake()
        {
            vfxSource = GetComponent<ParticleSystem>();
            sfxSource = GetComponent<AudioSource>();
        }

        public void OnParticleSystemStopped()
        {
            Deallocate();
        }

        public void Deallocate()
        {
            if (sfxSource && sfxSelector)
            {
                sfxSource.Stop();
                sfxSource.clip = null;
            }
            if (useVfx)
            {
                vfxSource.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
            ObjectPool.DeactivateObject(poolName, gameObject);
        }

        public void OnActivate()
        {
            if (sfxSource && sfxSelector)
            {
                sfxSource.clip = sfxSelector.GetSound();
                sfxSource.Play();
            }
            if(useVfx)
            {
                vfxSource.Play();
            }
        }
    }
}