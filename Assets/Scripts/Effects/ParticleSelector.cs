﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "New Particle Selector", menuName = "Carbon Vanguard/New Particle Selector", order = 81)]
    public class ParticleSelector : ScriptableObject
    {
        public ParticleSystem[] effects;

        private int count;
        private bool init = false;

        public ParticleSystem GetEffect()
        {
            if (!init)
            {
                count = effects.Length;
                init = true;
            }
            if (count == 1)
                return effects[0];

            return effects[Random.Range(0, count)];
        }
    }
}