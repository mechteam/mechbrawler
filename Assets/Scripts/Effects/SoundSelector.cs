﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "New Sound Selector", menuName = "Carbon Vanguard/New Sound Selector", order = 80)]
    public class SoundSelector : ScriptableObject
    {
        public AudioClip[] sounds;

        public AudioClip GetSound()
        {
            if (sounds.Length == 0)
                return null;

            return sounds[Random.Range(0, sounds.Length)];
        }
    }
}