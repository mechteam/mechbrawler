﻿using System.Collections.Generic;
using UnityEngine;


namespace CarbonVanguard
{
    public class ObjectPool : MonoBehaviour
    {
        public static ObjectPool Instance;

        public List<PooledItem> itemsInPool;
        private Dictionary<string, PooledItemData> _pooledGameObjects;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            _pooledGameObjects = new Dictionary<string, PooledItemData>();

            int itemIndex = 0;

            foreach (PooledItem item in itemsInPool)
            {
                LinkedList<GameObject> goList = new LinkedList<GameObject>();

                for (int i = 0; i < item.maxObjects; i++)
                {
                    GameObject newObj = Instantiate<GameObject>(item.original, null);
                    newObj.SetActive(false);

                    goList.AddFirst(newObj);
                }

                PooledItemData data = new PooledItemData(goList, itemIndex);

                _pooledGameObjects.Add(item.name, data);
                itemIndex++;
            }
        }

        /// <summary>
        /// Look for a currently unused object and activate it
        /// </summary>
        /// <param name="objectName">The name of the object collection to use</param>
        /// <param name="position">The desired position</param>
        /// <param name="rotation">The desired rotation</param>
        /// <param name="parent">The desired parent</param>
        /// <returns>A GameObject if successful and null if not</returns>
        public static GameObject RequestObject(string objectName, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (Instance != null)
            {
                if (Instance._pooledGameObjects.ContainsKey(objectName))
                {
                    PooledItemData itemData = Instance._pooledGameObjects[objectName];

                    foreach (GameObject go in itemData.gameObjects)
                    {
                        if (go.activeInHierarchy)
                        {
                            //This object is already in use
                            continue;
                        }
                        else
                        {
                            //Found one!
                            Transform goTransform = go.GetComponent<Transform>();
                            goTransform.position = position;
                            goTransform.rotation = rotation;
                            goTransform.parent = parent;
                            go.SetActive(true);

                            IObjectPooled goInterface = go.GetComponent<IObjectPooled>();

                            if(goInterface != null)
                                goInterface.OnActivate();

                            return go;
                        }
                    }

                    //Here, there are no avaliable objects. Try to auto-expand
                    if (Instance.itemsInPool[itemData.index].autoExpand)
                    {
                        GameObject expandObject = Instantiate<GameObject>(Instance.itemsInPool[itemData.index].original, position, rotation, parent);
                        itemData.gameObjects.AddFirst(expandObject);

                        IObjectPooled goInterface = expandObject.GetComponent<IObjectPooled>();

                        if (goInterface != null)
                            goInterface.OnActivate();

                        return expandObject;
                    }

                    //No avaliable objects and can't auto-expand
                    return null;
                }

                //Key doesn't exist
                return null;
            }

            //Instance doesn't exist
            return null;
        }

        public static T RequestObject<T>(string objectName, Vector3 position, Quaternion rotation, Transform parent = null)
            where T : Component
        {
            return RequestObject(objectName, position, rotation, parent).GetComponent<T>();
        }

        public static void DeactivateObject(string objectName, GameObject gameObject)
        {
            if (Instance != null)
            {
                if (Instance._pooledGameObjects.ContainsKey(objectName))
                {
                    PooledItemData data = Instance._pooledGameObjects[objectName];

                        if (data.gameObjects.Contains(gameObject))
                            gameObject.SetActive(false);
                }
            }
        }
    }
}