﻿using System;
using UnityEngine;

namespace CarbonVanguard
{
    [Serializable]
    public class PooledItem
    {
        public string name;
        public GameObject original;
        public uint maxObjects;
        public bool autoExpand;
    }
}