﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PooledItemData
{
    public LinkedList<GameObject> gameObjects;
    public int index;

    public PooledItemData(LinkedList<GameObject> objects, int ind)
    {
        gameObjects = objects;
        index = ind;
    }
}