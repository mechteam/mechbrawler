﻿using UnityEngine;

namespace CarbonVanguard
{
    public interface IObjectPooled
    {
        string poolName {get;set;}

        void Deallocate();
        void OnActivate();
    }
}