﻿using UnityEngine;

namespace CarbonVanguard
{
    public class TrackOnAxis : MonoBehaviour
    {
        public Transform trackingObject;

        public bool followX = true;
        public bool followY = true;
        //public bool followZ = true;

        public float speed;

        public float lowerClamp;
        public float upperClamp;

        private bool clamping = true;
        private bool followAll = false;

        private void Start()
        {
            if(trackingObject == null)
            {
                enabled = false;
                //throw new System.ArgumentNullException("Tracking Object");
            }

            if (followX && followY)
                followAll = true;

            if (lowerClamp == 0f && upperClamp == 0f)
                clamping = false;
        }

        private void Update()
        {
            if(followAll)
            {
                Vector3 fwd = trackingObject.position - transform.position;
                Quaternion towards = Quaternion.LookRotation(fwd);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, towards, speed * Time.deltaTime);
            }
            else if(followY)
            {
                Vector3 fwd = trackingObject.position - transform.position;
                //fwd = transform.InverseTransformDirection(fwd);
                float angle = Mathf.Atan2(fwd.x, fwd.z) * Mathf.Rad2Deg;
                if (clamping)
                    angle = Mathf.Clamp(angle, lowerClamp, upperClamp);
                Vector3 angles = transform.eulerAngles;
                angles.y = Mathf.MoveTowardsAngle(angles.y, angle, speed * Time.deltaTime);
                transform.eulerAngles = angles;
            }
            else if(followX)
            {
                Vector3 fwd = trackingObject.position - transform.position;
                //fwd = transform.InverseTransformDirection(fwd);
                float angle = -Mathf.Atan2(fwd.y, fwd.z) * Mathf.Rad2Deg;
                if (clamping)
                    angle = Mathf.Clamp(angle, lowerClamp, upperClamp);
                Vector3 angles = transform.eulerAngles;
                angles.x = Mathf.MoveTowardsAngle(angles.x, angle, speed * Time.deltaTime);
                transform.eulerAngles = angles;
            }
        }
    }
}