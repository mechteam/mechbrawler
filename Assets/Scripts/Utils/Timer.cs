﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public delegate void GenericEvent();
}

namespace CarbonVanguard.Utils
{
    public class Timer
    {
        public event GenericEvent TimerElapsed;
        public event GenericEvent TimerStarted;
        public event GenericEvent TimerStopped;

        public float interval;
        public bool isRunning = false;

        private MonoBehaviour ownerBehaviour;

        public Timer(MonoBehaviour owner)
        {
            interval = 1;
            ownerBehaviour = owner;
        }

        public Timer(float interval, MonoBehaviour owner)
        {
            this.interval = interval;
            ownerBehaviour = owner;
        }

        private IEnumerator TimerElapse(float t, bool useTimeScale, uint maxCalls = 0)
        {
            isRunning = true;
            int calls = 0;

            while (isRunning)
            {
                if (useTimeScale)
                    yield return new WaitForSeconds(t);
                else
                    yield return new WaitForSecondsRealtime(t);

                if (TimerElapsed != null)
                    TimerElapsed();

                if (maxCalls > 0)
                {
                    calls++;
                    if (calls >= maxCalls)
                    {
                        TimerStop();
                        yield return null;
                    }
                }
            }
        }

        public void TimerStop()
        {
            ownerBehaviour.StopCoroutine("TimerElapse");
            isRunning = false;
            if (TimerStopped != null)
                TimerStopped();
        }

        public void TimerOnce(bool respectTimeScale = true)
        {
            if (TimerStarted != null)
                TimerStarted();

            if (interval == 0)
            {
                TimerElapsed();
                return;
            }

            ownerBehaviour.StartCoroutine(TimerElapse(interval, respectTimeScale, 1));

           //TimerStop();
        }

        public void TimerRepeating(uint amount, bool respectTimeScale = true)
        {
            if (TimerStarted != null)
                TimerStarted();

            if (interval == 0)
            {
                TimerElapsed();
                return;
            }
                
            
                ownerBehaviour.StartCoroutine(TimerElapse(interval, respectTimeScale, amount));

            TimerStop();
        }

        public void TimerRepeating(bool respectTimeScale = true)
        {
            if (TimerStarted != null)
                TimerStarted();

            ownerBehaviour.StartCoroutine(TimerElapse(interval, respectTimeScale));
        }
    }
}