﻿using UnityEngine;

public class Screenshot : MonoBehaviour
{
    public KeyCode screenshotKey = KeyCode.P;
    public int sizeMultiplier = 1;

    private void Update()
    {
        if(Input.GetKeyDown(screenshotKey))
        {
            ScreenCapture.CaptureScreenshot(Application.dataPath + "/output.png", sizeMultiplier);
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log(Application.dataPath + "/output.png");
        }
    }
}
