﻿using System;
using System.Collections;
using UnityEngine;


namespace CarbonVanguard.Utils
{
    /// <summary>
    /// This is basically timer lite. Only a constructor that executes a coroutine on the specified MonoBehaviour
    /// </summary>
    public class Delay
    {
        public Delay(MonoBehaviour behaviour, Action function, float delay)
        {
            if (behaviour != null)
            {
                behaviour.StartCoroutine(Start(behaviour, function, delay));
            }
        }

        private IEnumerator Start(MonoBehaviour mb, Action func, float delay)
        {
            yield return new WaitForSeconds(delay);
            func();
            mb.StartCoroutine("Start");
        }
    }
}