﻿using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    public static class UIUtils
    {
        static UIUtils() { }

        public static void UpdateImage(Image img, Stat value, Gradient colorGradient = null)
        {
            float pct = value.Percentage;

            if ((img.fillMethod & (Image.FillMethod.Horizontal | Image.FillMethod.Vertical)) != 0)
            {
                img.fillAmount = pct;
            }

            if (colorGradient != null)
                img.color = colorGradient.Evaluate(pct);
        }
    }
}
