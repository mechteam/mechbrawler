﻿using UnityEngine;
using UnityEngine.UI;

namespace CarbonVanguard
{
    [RequireComponent(typeof(Image))]
    public class CycleFrames : MonoBehaviour
    {

        [Tooltip("All the frames (in order) of the animation")]
        public Sprite[] frames;
        [Tooltip("The frames per second of the animation")]
        public int FPS = 10;
        [Tooltip("Tick if this should loop forever")]
        public bool loopInfinite = true;
        [Tooltip("How many loops to go through")]
        public int loopCount = 0;
        [Tooltip("Should the object destroy itself when finished?")]
        public bool destroyOnFinish = true;

        //Current amount of times this has looped.
        private int cycles = 0;
        //The attached image
        private Image display;

        private void Awake()
        {
            //If infinite is true, set the amount of loops to the maximum value of an int
            //While not truely repeating forever, this should not be an issue in gameplay
            if (loopInfinite)
                loopCount = int.MaxValue;

            display = gameObject.GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            //If there are still cycles to go
            if (cycles < loopCount)
            {
                //The current index is the game time (as an int) multiplied by the frames per second as a remainder of the amount of frames in the sprite array
                int index = (int)(Time.time * FPS) % frames.Length;
                display.sprite = frames[index];
                //If the current index is at the end of the array, the index becomes 0, restarting the loop.
                if (index == frames.Length)
                {
                    //Add one to cycles to count that we have looped through it again
                    cycles++;
                    index = 0;
                }
            }
            //If destruction is desired and there are no more cycles to go, destroy.
            else if (destroyOnFinish)
                Destroy(gameObject);
        }
    }
}