﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard.Utils
{
    public class RouletteSelector<T>
    {
        private List<T> initial;
        private List<T> activeList;
        private List<T> inactiveList;

        public RouletteSelector(List<T> selection)
        {
            initial = selection;
            Reset();
        }

        private void Reset()
        {
            activeList = new List<T>(initial);
            inactiveList = new List<T>(activeList.Count);
        }

        public T Select()
        {
            T sel = activeList[Random.Range(0, activeList.Count)];

            if (activeList.Count == 0)
            {
                Reset();
            }

            activeList.Remove(sel);
            inactiveList.Add(sel);

            return sel;
        }
    }
}