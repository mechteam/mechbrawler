﻿//#define NO_TARGET
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace CarbonVanguard
{
    [RequireComponent(typeof(NavMeshAgent))]
    [DisallowMultipleComponent]
    public class AIAgent : Entity
    {
        public Mech player;
        public static bool noTarget = false;

        public bool useOverrideHeight;
        public Transform overrideHeight;

        public EquipmentSlot equipment;
        public Weapon weaponData;

        public Transform firingPoint;

        public Vector3 inaccuracy;
        public Vector3 movementVariance;

        public float sightRange;
        public float alertRange;
        public float reactionTime;
        public float scanTime;

        public string dieEffectName;
        public float deathEffectUpMulti;

        private Animator animController;
        private NavMeshAgent navAgent;

        private Utils.Timer reaction;
        private Utils.Timer scanTimer;

        private Utils.RouletteSelector<MechPart> partSelector;

        private MechPart targetPart;

        private bool alert;

        public override void Reset() { }

        private void OnEnable()
        {
            if (player == null)
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Mech>();

            if (navAgent == null)
                navAgent = GetComponent<NavMeshAgent>();

            if (reaction == null)
                reaction = new Utils.Timer(this);

            if (scanTimer == null)
                scanTimer = new Utils.Timer(this);

            if (partSelector == null)
                partSelector = new Utils.RouletteSelector<MechPart>(player.GetParts());

            if (animController == null)
                animController = GetComponent<Animator>();

            reaction.TimerElapsed += AttackPlayer;
            scanTimer.TimerElapsed += SearchForPlayer;

            reaction.interval = reactionTime;
            scanTimer.interval = scanTime;

            scanTimer.TimerRepeating();

            equipment.data = weaponData;
            equipment.Startup();

#if NO_TARGET
            noTarget = true;
#endif
        }

        private void Update()
        {
            if(animController != null)
            {
                if(navAgent.remainingDistance <= 0.1f)
                {
                    animController.SetBool("Walking", false);
                }
                else
                {
                    animController.SetBool("Walking", true);
                }
            }
        }

        private void OnDisable()
        {
            reaction.TimerElapsed -= AttackPlayer;
            scanTimer.TimerElapsed -= SearchForPlayer;
        }

        private void Start()
        {
            health.OnValueChanged += AlertOthers;
        }

        private void AlertOthers()
        {
            if (alert)
                return;

            alert = true;
            Collider[] agentsToAlert = Physics.OverlapSphere(transform.position, alertRange);

            if (agentsToAlert.Length > 0)
            {
                for (int i = 0; i < agentsToAlert.Length; i++)
                {
                    AIAgent alertTarget = agentsToAlert[i].GetComponent<AIAgent>();

                    if (alertTarget != null)
                        alertTarget.alert = true;
                }
            }
        }

        private void AttackPlayer()
        {
            if (player == null || noTarget)
                return;

            if (targetPart == null)
            {
                targetPart = partSelector.Select();
            }

            Vector3 dir = (targetPart.transform.position - firingPoint.position).normalized;

            firingPoint.LookAt(targetPart.transform);
            firingPoint.Rotate(Random.insideUnitSphere + inaccuracy);

            if (Physics.Raycast(firingPoint.position, dir, out RaycastHit h, 1000, Physics.AllLayers))
            {
                if (h.collider.gameObject == targetPart.gameObject)
                {   
                    if(animController != null)
                        animController.SetBool("Shooting", true);
                    equipment.Use();

                    if (targetPart.health.Current == targetPart.health.Min)
                    {
                        targetPart = null;
                    }
                }
                else if(animController != null)
                {
                    animController.SetBool("Shooting", false);
                }
            }
        }

        private void SearchForPlayer()
        {
            if (player == null || noTarget)
                return;

            foreach(Collider col in Physics.OverlapSphere(transform.position, sightRange))
            {
                if (col.gameObject == player.gameObject || alert)
                {
                    reaction.TimerOnce();

                    if (useOverrideHeight && overrideHeight != null)
                    {
                        Vector3 navPos = player.transform.position + (Random.insideUnitSphere + movementVariance);
                        navPos.y = overrideHeight.position.y;

                        navAgent.SetDestination(navPos);
                        return;
                    }

                    navAgent.SetDestination(player.transform.position + (Random.insideUnitSphere + movementVariance));
                    return;
                }
            }
        }

        protected override void OnDie()
        {
            if(!string.IsNullOrWhiteSpace(dieEffectName))
            {
                ObjectPool.RequestObject(dieEffectName, transform.position + Vector3.up * deathEffectUpMulti, Quaternion.identity);
            }
            base.OnDie();
        }
    }
}