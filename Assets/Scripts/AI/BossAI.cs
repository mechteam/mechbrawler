﻿using UnityEngine;
using UnityEngine.AI;

namespace CarbonVanguard
{
    public class BossAI : MonoBehaviour
    {
        public Mech target;

        public Weapon heavyWeapon;
        public HitscanWeapon meleeWeapon;

        public float evaluateDelay;

        public float heavyUseDistance;
        public float chargeDistance;

        public float chargeTime;
        public float heavyUseTime;

        private NavMeshAgent navAgent;
        private Animator animController;

        private Utils.Timer evalulateTimer;

        void Awake()
        {
            navAgent = GetComponent<NavMeshAgent>();
            animController = GetComponent<Animator>();
            evalulateTimer = new Utils.Timer(evaluateDelay, this);
            evalulateTimer.TimerElapsed += Evaulate;
        }

        void Evaulate()
        {
            float distanceToTarget = Vector3.Distance(transform.position, target.transform.position);

            if (distanceToTarget >= heavyUseDistance)
            {
                navAgent.isStopped = true;
                animController.SetTrigger("Heavy");
                //Use heavy

                Invoke("FinishAttack", heavyUseTime);
            }
            else if(distanceToTarget >= chargeDistance)
            {
                navAgent.isStopped = true;
                animController.SetTrigger("Charge");
                //Use charge

                Invoke("FinishAttack", chargeTime);
            }
            else
            {
                navAgent.SetDestination(target.transform.position);

                if(distanceToTarget <= meleeWeapon.range)
                {
                    //Smacc the player
                }
            }
        }

        void FinishAttack()
        {
            navAgent.isStopped = false;
            evalulateTimer.TimerRepeating();
        }
    }
}