﻿using UnityEngine.Events;

namespace CarbonVanguard
{
    public interface IObjectiveTarget
    {
        UnityEvent Progress { get; set; }
    }
}