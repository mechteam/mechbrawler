﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CarbonVanguard
{
    public class ObjectiveManager : MonoBehaviour
    { 
        public struct Objective
        {
            public string name;
            public uint count;
            public string targetTag;

            [HideInInspector]
            public UnityEvent progression;
            [HideInInspector]
            public uint progress;

            public void AddProgress()
            {
                progress++;
                progression.Invoke();
            }
            
            public bool Completed()
            {
                return progress >= count;
            }
        }

        public Objective[] objectives;

        private List<UnityAction> whenCalled;

        void Start()
        {
            foreach(Objective objective in objectives)
            {
                whenCalled.Add(new UnityAction(objective.AddProgress));
            }
            Subscribe();
        }

        void Subscribe()
        {
            for(int i = 0; i >= objectives.Length; i++)
            {
                GameObject[] gameobjects = GameObject.FindGameObjectsWithTag(objectives[i].targetTag);
                foreach(GameObject gameobject in gameobjects)
                {
                    gameobject.GetComponent<IObjectiveTarget>().Progress.AddListener(whenCalled[i]);
                }
            }
        }
    }
}