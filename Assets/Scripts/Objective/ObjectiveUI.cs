﻿using UnityEngine;
using TMPro;

namespace CarbonVanguard
{
    public class ObjectiveUI : MonoBehaviour
    {
        public ObjectiveManager objectiveManager;
        public TextMeshPro textDisplay;

        public bool showProgress;
        public string progressDivider;

        void Start()
        {
            if(objectiveManager == null)
            {
                objectiveManager = FindObjectOfType(typeof(ObjectiveManager)) as ObjectiveManager;
                if (objectiveManager == null)
                    throw new System.NullReferenceException("No objective manager exists. Please put one in the scene.");
            }

            UpdateDisplay();
            Subscribe();
        }

        void UpdateDisplay()
        {
            string toInsert = "";

            foreach(ObjectiveManager.Objective objective in objectiveManager.objectives)
            {
                toInsert += objective.name;
                if(showProgress)
                {
                    toInsert += " " + objective.progression;
                    toInsert += progressDivider;
                    toInsert += " " + objective.count;
                }
                toInsert += "\n";
            }

            textDisplay.text = toInsert;
        }

        void Subscribe()
        {
            foreach(ObjectiveManager.Objective objective in objectiveManager.objectives)
            {
                objective.progression.AddListener(UpdateDisplay);
            }
        }
    }
}