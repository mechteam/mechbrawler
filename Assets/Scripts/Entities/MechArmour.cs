﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "Armour", menuName = "Carbon Vanguard/Armour")]
    public class MechArmour : MechEquipment
    {
        public float armour;

        public override void UseEquipment(params object[] options) {}
    }
}