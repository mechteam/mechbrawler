﻿using UnityEngine;

namespace CarbonVanguard
{
    public abstract class MechEquipment : ScriptableObject
    {
        public string uiName;
        [Multiline]
        public string uiDesc;
        [Tooltip("Reload speed for light weapons, cooldown for heavy, fuel for utilities")]
        public float cooldown;
        public GameObject model;
        public int cost;
        public abstract void UseEquipment(params object[] options);
    }
}