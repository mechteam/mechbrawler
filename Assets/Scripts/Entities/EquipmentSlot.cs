﻿using UnityEngine;

namespace CarbonVanguard
{
    public class EquipmentSlot : MonoBehaviour, IResettable
    {
        public CharInput input;
        public MechEquipment data;
        [Tooltip("The transform where the weapon model will be applied to.")]
        public Transform weaponFixPosition;
        public Transform bulletSpawn;

        public Transform frame;

        public Stat resource;
        public Stat cooldown;

        public bool left;
        public bool isHeavy;
        public bool hitscan;

        private GameObject createdObject;
        private Animator objectAnim;
        private bool isWeapon;
        private bool weaponReady;
        private bool isFiring;
        public bool usingUtil;

        private bool reachedTurn = false;
        private bool going = false;

        private Weapon wep;
        private MechUtility util;

        private Utils.Timer weaponTimer;
        private Utils.Timer reloadTimer;
        private Utils.Timer useTimer;

        private Quaternion frameOriginalRotation;

        public bool reloading { get; private set; }

        public void Startup()
        {
            if (data == null)
                return;
            wep = data as Weapon;
            if (wep != null)
                Setup(gameObject.transform, wep);
            else
                Setup(gameObject.transform, data as MechUtility);

            if (objectAnim != null)
                objectAnim.SetFloat("Left", left ? 1 : 0);

            if (frame != null)
                frameOriginalRotation = frame.localRotation;
        }

        private void Setup(Transform parent, Weapon weapon)
        {
            isWeapon = true;
            data = weapon;
            resource = new Stat(0, weapon.ammo);

            if (createdObject != null)
                Destroy(createdObject);
            if (data.model != null && weaponFixPosition != null)
            {
                createdObject = Instantiate(data.model, weaponFixPosition.position, weaponFixPosition.rotation, weaponFixPosition);
                objectAnim = createdObject.GetComponent<Animator>();
                if (objectAnim != null)
                    objectAnim.Rebind();
            }

            if (!isHeavy)
                weaponTimer = new Utils.Timer(weapon.shotDelay, this);
            else
                weaponTimer = new Utils.Timer(data.cooldown, this);

            useTimer = new Utils.Timer(wep.useDelay, this);
            useTimer.TimerElapsed += UseStandardWepon;

            weaponTimer.TimerElapsed += WeaponReady;

            reloadTimer = new Utils.Timer(weapon.cooldown, this);
            reloadTimer.TimerElapsed += Reload;
            reloadTimer.TimerStarted += StopAnims;

            Reset();
        }

        private void Setup(Transform parent, MechUtility utility)
        {
            isWeapon = false;
            data = utility;

            if (createdObject != null)
                Destroy(createdObject);

            if (utility.model != null)
                createdObject = Instantiate(data.model, weaponFixPosition.position, weaponFixPosition.rotation, weaponFixPosition);

            Reset();
        }

        private void WeaponReady()
        {
            weaponReady = true;
        }

        private void Reload()
        {
            resource.Current = resource.Max;
            weaponReady = true;
            reloading = false;

            StopAnims();
        }

        public void ManualReload()
        {
            if (resource.Current == resource.Max)
                return;

            resource.Current = resource.Min;
        }

        public void Reset()
        {
            cooldown = new Stat(0, data.cooldown);

            wep = data as Weapon;
            util = data as MechUtility;

            if (wep != null)
                resource = new Stat(0, wep.ammo);

            if (util != null)
                resource = new Stat(0, util.cooldown);
            WeaponReady();
        }

        public void StopTimer(ref Utils.Timer tmr)
        {
            tmr.TimerStop();
        }


        private void UseStandardWepon()
        {
            if (isWeapon)
            {
                resource.Current -= wep.ammoPerShot;

                isFiring = true;

                if (input != null)
                {
                    bulletSpawn.LookAt(input.playerView.ViewportToWorldPoint(new Vector3(0.5f, 0.52f, 100)));
                }
                
                wep.UseEquipment(bulletSpawn);

                if (weaponTimer != null)
                {
                    weaponTimer.TimerOnce();
                    weaponReady = false;
                }
                if (objectAnim != null)
                {
                    objectAnim.SetBool("Use", true);
                    objectAnim.SetBool("Stop", false);
                }

                going = true;
                Invoke("SlideBack", wep.resetTime);

                isFiring = false;
            }
            else
            {
                if (resource.Current > resource.Min)
                {
                    usingUtil = true;
                    resource.Current -= util.resourceUsage * Time.deltaTime;
                    util.UseEquipment();

                    isFiring = false;
                }
            }
        }
        
        public void Use()
        {
            if (isWeapon && (!weaponReady || reloading))
                return;

            if (data == null)
                return;

            if (wep == null || wep.useDelay == 0)
                UseStandardWepon();
            else if(!useTimer.isRunning)
                useTimer.TimerOnce();
        }

        public void StopAnims()
        {
            if (objectAnim != null)
            {
                objectAnim.SetBool("Use", false);
                objectAnim.SetBool("Stop", true);
            }
        }

        public bool Ready
        {
            get
            {
                return weaponReady && !reloading;
            }
        }

        private void Update()
        {
            if (isWeapon)
            {
                if (wep == null)
                    return;

                if (resource.Current < wep.ammoPerShot && !reloading)
                {
                    if (!reloadTimer.isRunning)
                        reloadTimer.TimerOnce();

                    reloading = true;
                    weaponReady = false;
                    return;
                }

                if (frame != null)
                {
                    if (going)
                    {
                        frame.localRotation = Quaternion.Lerp(frame.localRotation, Quaternion.Euler(left ? wep.shakeAmount : -wep.shakeAmount), wep.lerpTo);
                    }
                    else if(reachedTurn)
                    {
                        frame.localRotation = Quaternion.Lerp(frame.localRotation, frameOriginalRotation, wep.lerpBack);
                    }
                }
            }
            else
            {
                if (util == null)
                    return;

                if (CharInput.isGrounded && !usingUtil)
                {
                    resource.Current += util.rechargeRate * Time.deltaTime;
                }
            }
        }

        private void SlideBack()
        {
            going = false;
            reachedTurn = true;
        }

        public override string ToString()
        {
            return data != null ? data.uiName : "None";
        }
    }
}