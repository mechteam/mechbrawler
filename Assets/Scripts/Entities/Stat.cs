﻿using System;
using UnityEngine;

namespace CarbonVanguard
{
    [Serializable]
    public class Stat : IResettable
    {
        public event GenericEvent OnValueChanged;
        [SerializeField]
        private float maxValue;
        [SerializeField]
        private float minValue;
        [SerializeField]
        private float currentValue;

        public Stat(Stat cpy)
        {
            maxValue = cpy.maxValue;
            minValue = cpy.minValue;
            currentValue = cpy.currentValue;
        }

        public Stat(float min, float max, float current)
        {
            if (min > max)
                throw new InvalidOperationException("Minimum value is greater than maximum");

            if (max < min)
                throw new InvalidOperationException("Maximum value is less than minimum");

            minValue = min;
            maxValue = max;
            currentValue = current;

            ClampCurrent();
        }

        public Stat(float min, float max, bool startAtMin = false)
        {
            minValue = min;
            maxValue = max;

            if (startAtMin)
                currentValue = min;
            else
                currentValue = max;
        }

        public float Max
        {
            get
            {
                return maxValue;
            }

            set
            {
                if (value < minValue)
                    value = (minValue + 1);

                maxValue = value;
                ClampCurrent();
            }
        }

        public float Min
        {
            get
            {
                return minValue;
            }

            set
            {
                if (value > maxValue)
                    value = maxValue - 1;

                minValue = value;
                ClampCurrent();
            }
        }

        public float Current
        {
            get
            {
                return currentValue;
            }

            set
            {
                currentValue = Mathf.Clamp(value, minValue, maxValue);

                if (OnValueChanged != null)
                    OnValueChanged();
            }
        }

        public float Percentage
        {
            get
            {
                return currentValue / maxValue;
            }
        }

        public static Stat operator+(Stat a, float b)
        {
            Stat result = new Stat(a.minValue, a.maxValue, a.currentValue + b);
            return result;
        }

        public static Stat operator +(Stat a, Stat b)
        {
            Stat result = new Stat(a.minValue, a.maxValue, a.currentValue + b.currentValue);
            return result;
        }

        public static Stat operator -(Stat a, Stat b)
        {
            Stat result = new Stat(a.minValue, a.maxValue, a.currentValue - b.currentValue);
            return result;
        }

        public static Stat operator -(Stat a, float b)
        {
            Stat result = new Stat(a.minValue, a.maxValue, a.currentValue - b);
            return result;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", currentValue, maxValue);
        }

        private void ClampCurrent()
        {
            currentValue = Mathf.Clamp(currentValue, minValue, maxValue);
        }

        public void Reset()
        {
            OnValueChanged = null;
            currentValue = maxValue;
        }
    }
}