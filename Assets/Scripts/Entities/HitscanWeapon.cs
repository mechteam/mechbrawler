﻿//#define DEBUG_HSWEAPON
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public enum HitscanType
    {
        Raycast,
        Boxcast,
        Spherecast
    }

    [CreateAssetMenu(fileName = "Hitscan Weapon", menuName = "Carbon Vanguard/Hitscan Weapon")]
    public class HitscanWeapon : Weapon
    {
        [Header("Hitscan Settings")]
        public LayerMask hitMask;
        public HitscanType hitscanType;
        public float range;
        public float width;

        public Vector2 extents;

        private void DamageEntity(RaycastHit info)
        {
            Entity target = info.collider.GetComponent<Entity>();

            if (target != null)
                target.TakeDamage(damage);
        }

        private void HitscanRay(Transform  pos)
        {
#if DEBUG_HSWEAPON
            DebugRay(pos, pos.position, pos.forward, range);
#endif
            if (Physics.Raycast(pos.position, pos.forward, out RaycastHit info, range, hitMask))
            {
                DamageEntity(info);
            }
        }

        private void HitscanSphere(Transform pos)
        {
#if DEBUG_HSWEAPON
            DebugSphere(pos, pos.position, pos.forward, width, range);
#endif

            if (Physics.SphereCast(pos.position, width, pos.forward, out RaycastHit info, range, hitMask))
            {
                DamageEntity(info);
            }
        }

        private void HitscanBox(Transform pos)
        {
#if DEBUG_HSWEAPON
            Vector3 debugBoxExtents = new Vector3(extents.x / 2, extents.y / 2, 1);

            DebugBox(pos, pos.position, debugBoxExtents, pos.forward, range);
#endif

            if (Physics.BoxCast(pos.position, new Vector3(extents.x / 2, extents.y / 2, 1), pos.forward, out RaycastHit info, pos.localRotation, range, hitMask))
            {
                DamageEntity(info);
            }
        }

#if DEBUG_HSWEAPON
        private void DebugBox(Transform t, Vector3 origin, Vector3 direction, Vector3 halfExtents, float dist)
        {
            for (int i = 0; i < 3; i++)
            {
                Vector3 start = origin;
                start[i] += halfExtents[i] * 2;
                Debug.DrawLine(start, start + (direction* dist), Color.green, 10);
            }
        }

        private void DebugRay(Transform t, Vector3 origin, Vector3 direction, float dist)
        {
            Debug.DrawLine(origin, origin + (direction * dist), Color.green, 10);
        }

        private void DebugSphere(Transform t, Vector3 origin, Vector3 direction, float radius, float dist)
        {
            direction = t.TransformDirection(direction);

            Vector3 startP = origin + direction * radius;
            Vector3 startN = origin + direction * -radius;

            Debug.DrawLine(startP, startP + (direction * dist), Color.green, 10);
            Debug.DrawLine(startN, startN + (direction * dist), Color.green, 10);
        }
#endif

        public override void UseEquipment(params object[] options)
        {
            if (options.Length != 0)
            {
                if (options[0] is Transform firingPoint)
                {
                    switch (hitscanType)
                    {
                        case HitscanType.Raycast:
                            {
                                HitscanRay(options[0] as Transform);
                                break;
                            }

                        case HitscanType.Boxcast:
                            {
                                HitscanBox(options[0] as Transform);
                                break;
                            }

                        case HitscanType.Spherecast:
                            {
                                HitscanSphere(options[0] as Transform);
                                break;
                            }

                        default:
                            {
                                HitscanRay(options[0] as Transform);
                                break;
                            }
                    }
                }
            }
        }
    }
}