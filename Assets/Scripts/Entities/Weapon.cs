﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "New Weapon", menuName = "Carbon Vanguard/New Weapon", order = 75)]
    public class Weapon : MechEquipment
    {
        [Header("Weapon Stats")]
        [Tooltip("Ensure this matches the name in the ObjectPool")]
        public string projectileName;
        public int ammo;
        public int ammoPerShot;
        [Tooltip("Number of projectiles per row")]
        public int projectileCount;
        public float damage;
        public float velocity;
        public Vector2 displacement;
        public Vector3 spread;
        public float rows;

        [Header("Effects")]
        public Vector3 shakeAmount;
        [Range(0, 1)] public float lerpTo;
        [Range(0, 1)] public float lerpBack;
        public float resetTime;

        [Header("UI")]
        public Sprite weaponCrosshair;

        [Tooltip("This only applies when attached to a player, AIs have their own delay property")]
        public float shotDelay;
        public float useDelay;

        public string fireEffectName;

        public override void UseEquipment(params object[] options)
        {
            if (options.Length == 0)
                throw new System.Exception("No bullet spawn defined");

            Transform pos = options[0] as Transform;
            for (int v = 0; v < rows; v++)
            {
                for (int h = 0; h < projectileCount; h++)
                {
                    Vector3 disp = new Vector3(displacement.x * h, displacement.y * v, 0);
                    Vector3 randSpread = Vector3.Cross(Random.insideUnitSphere, spread);

                    pos.Rotate(randSpread);

                    if ((h % 2) == 0)
                        disp = -disp;

                    if (fireEffectName != "")
                    {
                        ObjectPool.RequestObject(fireEffectName, pos.position, pos.rotation, pos);
                    }
                    ObjectPool.RequestObject<Projectile>(projectileName, pos.position + disp, pos.rotation).Launch(pos.forward, velocity, damage);
                }
            }
        }
    }
}