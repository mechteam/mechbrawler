﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : MonoBehaviour
    {
        public float damage;
        public string projectileName;

        [Tooltip("Ensure this matches the name in the Object Pool")]
        public string effectName;

        private Rigidbody rb;

        public void Launch(Vector3 direction, float velocity, float damage)
        {
            rb = GetComponent<Rigidbody>();
            rb.AddForce(direction * velocity);
            this.damage = damage;
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            Entity target = other.GetComponent<Entity>();

            if (target != null)
                target.TakeDamage(damage);

            if(effectName != null)
            {
                ObjectPool.RequestObject(effectName, transform.position, transform.rotation);
            }

            Reset();


            ObjectPool.DeactivateObject(projectileName, gameObject);
        }

        protected virtual void Reset()
        {
            rb.velocity = Vector3.zero;
            damage = 0f;
            TrailRenderer tr = GetComponent<TrailRenderer>();
            if (tr)
                tr.Clear();
        }
    }
}