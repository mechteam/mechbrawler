﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CarbonVanguard
{
    public class CharCmd : CVInput.IPlayerActions
    {
        public static bool holdAim;

        public static Vector2 moveInput { get; private set; }
        public static Vector2 lookInput { get; private set; }

        public static bool heavyWeapon { get; private set; }
        public static bool leftWeapon { get; private set; }
        public static bool rightWeapon { get; private set; }
        public static bool utilityItem { get; private set; }
        public static bool reloadLeft { get; private set; }
        public static bool reloadRight { get; private set; }
        public static bool reloadBoth { get; private set; }
        public static bool showObjective { get; private set; }
        public static bool zoomToggle { get; private set; }
        public static bool pauseToggle { get; private set; }

        public CharCmd()
        {
            holdAim = false;
            //TODO: Get static settings from options file
        }

        public override string ToString()
        {
            return string.Format("Move Vec: {0}\nLook Vec: {1}\nLeft Weapon: {2}\nRight Weapon: {3}\nHeavy Weapon: {4}\nUtility: {5}\nReload Left: {6}\nReload Right: {7}\n Reload Both: {8}\nShow Objective: {9}\nZoom: {10}\nPause: {11}",
                                moveInput, lookInput, leftWeapon, rightWeapon, heavyWeapon, utilityItem, reloadLeft, reloadRight, reloadBoth, showObjective, zoomToggle, pauseToggle);
        }

        #region Callbacks
        public void OnHeavyWeapon(InputAction.CallbackContext context)
        {
            heavyWeapon = !heavyWeapon;
        }

        public void OnLeftWeapon(InputAction.CallbackContext context)
        {
            leftWeapon = context.control.IsActuated();
        }

        public void OnLook(InputAction.CallbackContext context)
        {
            lookInput = context.ReadValue<Vector2>();
        }

        public void OnMobilityItem(InputAction.CallbackContext context)
        {
            utilityItem = context.control.IsActuated();
        }

        public void OnMovement(InputAction.CallbackContext context)
        {
            moveInput = context.ReadValue<Vector2>();
        }

        public void OnPause(InputAction.CallbackContext context)
        {
            if (context.control.IsActuated())
                pauseToggle = !pauseToggle;
        }

        public void OnReloadBoth(InputAction.CallbackContext context)
        {
            reloadBoth = context.control.IsActuated();
        }

        public void OnReloadLeft(InputAction.CallbackContext context)
        {
            reloadLeft = context.control.IsActuated(); 
        }

        public void OnReloadRight(InputAction.CallbackContext context)
        {
            reloadRight = context.control.IsActuated();
        }

        public void OnRightWeapon(InputAction.CallbackContext context)
        {
            rightWeapon = context.control.IsActuated();
        }

        public void OnToggleObjective(InputAction.CallbackContext context)
        {
            if (context.control.IsActuated())
                showObjective = !showObjective;
        }

        public void OnZoom(InputAction.CallbackContext context)
        {
            zoomToggle = context.ReadValue<float>() != 0;
        }
        #endregion
    }
}