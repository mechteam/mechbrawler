﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "New Utility", menuName = "Carbon Vanguard/New Equipment", order = 75)]
    public class MechUtility : MechEquipment
    {
        [Tooltip("How the utility will affect the velocity of the mech")]
        public float resourceUsage;
        public float rechargeRate;
        public Vector3 velocityModifier;

        public override void UseEquipment(params object[] options)
        {
            CharInput.Impulse(velocityModifier * Time.deltaTime);
        }
    }
}