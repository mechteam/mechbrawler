﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public enum PartType
    {
        Torso,
        Arm,
        Leg,
        Armour
    }

    public class MechPart : Entity
    {
        public PartType partType;
        public EquipmentSlot equipment;
        public bool left;
        public bool excludeFromCustomiser;
        public int attatchIndex;
        public event EntityEvent MechDamageEvent;

        public float armour;

        public override void TakeDamage(float amount)
        {
            MechDamageEvent(this, amount);
        }

        public void Use()
        {
            equipment.Use();
        }

        public void Stop()
        {
            equipment.StopAnims();
        }

        public void Startup()
        {
            if (equipment != null)
            {
                equipment.Startup();
                equipment.left = left;
            }
                

            Reset();
        }

        public override void Reset()
        {
            health.Reset();
        }
    }
}