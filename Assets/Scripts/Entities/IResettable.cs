﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public interface IResettable
    {
        void Reset();
    }
}