﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CarbonVanguard
{
    public class Mech : MonoBehaviour, IResettable
    {
        [Header("Parts and Stats")]
        public MechPart leftLeg;
        public MechPart rightLeg;
        public MechPart leftArm;
        public MechPart rightArm;
        public MechPart torso;
        public MechPart armourPart;
        public Stat energyShield;
        public float shieldRechargeWait;
        public float shieldRechargeSpeed;
        public float shieldRechargeAmount;

        public ImageFade gameOverFade;
        public ImageFade textFade;
        private bool ded;

        private List<MechPart> parts;
        private Utils.Timer shieldRechargeTimer;
        private Utils.Timer shieldRechargeDelta;

        private bool recharge;

        public void MechDamageHandler(Entity part, params object[] data)
        {
            float damage = (float)data[0];

            if (energyShield.Current > 0)
            {
                if (energyShield.Current > damage)
                {
                    energyShield.Current -= damage;
                }
                else
                {
                    float diff = damage - energyShield.Current;
                    energyShield.Current = 0;
                    part.health.Current -= diff;
                }
            }
            else
            {
                part.health.Current -= damage;
            }

            StopRecharge();
            shieldRechargeDelta.TimerStop();
            shieldRechargeTimer.TimerStop();

            shieldRechargeTimer.TimerOnce();

            if (torso.health.Current <= torso.health.Min)
                ded = true;
        }

        public void Equip(EquipmentSlot slot, MechEquipment equipmentToAdd)
        {
            slot.data = equipmentToAdd;

            slot.Startup();
        }

        public bool HasEquipment<T>() where T : MechEquipment
        {
            MechEquipment toReturn;
            if (toReturn = leftArm.GetComponent<MechEquipment>())
                return toReturn;
            if (toReturn = rightArm.GetComponent<MechEquipment>())
                return toReturn;
            if (toReturn = torso.GetComponent<MechEquipment>())
                return toReturn;
            if (toReturn = leftLeg.GetComponent<MechEquipment>())
                return toReturn;
            if (toReturn = rightLeg.GetComponent<MechEquipment>())
                return toReturn;

            return false;
        }

        public MechPart GetPart(int index)
        {
            if (parts == null)
                return null;

            return parts[index];
        }

        public List<MechPart> GetParts()
        {
            if (parts == null)
            {
                parts = new List<MechPart>()
                {
                    torso,
                    leftArm,
                    rightArm,
                    leftLeg,
                    rightLeg,
                    armourPart
                };
            }

            return parts;
        }

        private void KickToMenu()
        {
            Destroy(gameObject);
            SceneManager.LoadScene(0);
        }

        public void AssignArmour(MechPart newArmour)
        {
            //if (armourPart != null)
            //{
            //    foreach (MechPart part in GetParts())
            //    {
            //        //part.health.Max -= armourPart.armour;
            //    }
            //}

            armourPart = newArmour;

            foreach (MechPart part in GetParts())
            {
                part.health.Max = newArmour.armour;
                part.health.Current = newArmour.armour;
            }
        }

        public void GameOver()
        {
            CharInput input = GetComponent<CharInput>();

            if (input != null)
                input.enabled = false;

            gameOverFade.enabled = true;
            textFade.enabled = true;

            Reset();

            Invoke("KickToMenu", 10);
        }


        private void Start()
        {
            DontDestroyOnLoad(this);
            Startup();
        }

        private void StartRecharge()
        {
            recharge = true;
            shieldRechargeDelta.TimerRepeating();
        }

        private void StopRecharge()
        {
            recharge = false;
        }

        private void RechargeLoop()
        {
            if(recharge)
                energyShield.Current += shieldRechargeAmount;
        }

        private void Startup()
        {
            leftArm.Startup();
            rightArm.Startup();
            rightLeg.Startup();
            leftLeg.Startup();
            torso.Startup();
            Reset();

            leftArm.left = true;
            rightArm.left = false;

            shieldRechargeTimer = new Utils.Timer(shieldRechargeWait, this);
            shieldRechargeDelta = new Utils.Timer(shieldRechargeSpeed, this);

            shieldRechargeTimer.TimerElapsed += StartRecharge;

            shieldRechargeDelta.TimerElapsed += RechargeLoop;
            shieldRechargeDelta.TimerStopped += StopRecharge;

            foreach (MechPart p in parts)
            {
                p.MechDamageEvent += MechDamageHandler;
            }

            if (gameOverFade != null && gameOverFade.enabled)
                gameOverFade.enabled = false;

            if (textFade != null && textFade.enabled)
                textFade.enabled = false;
        }

        public void Reset()
        {
            leftArm.Reset();
            rightArm.Reset();
            rightLeg.Reset();
            leftLeg.Reset();
            torso.Reset();

            ded = false;

            energyShield.Reset();
        }

        private void Update()
        {
            if (ded)
                GameOver();
        }
    }
}