﻿using UnityEngine;

namespace CarbonVanguard
{
    [CreateAssetMenu(fileName = "New Melee Weapon", menuName = "Carbon Vanguard/New Melee Weapon", order = 75)]
    public class MeleeWeapon : Weapon
    {
        public float range;
        public float damageAreaSize;

        public override void UseEquipment(params object[] options)
        {
            Transform[] spawnPoints = options[0] as Transform[];

            foreach(RaycastHit hit in Physics.BoxCastAll(spawnPoints[0].position, new Vector3(damageAreaSize, damageAreaSize, damageAreaSize), spawnPoints[0].forward, spawnPoints[0].rotation, range))
            {
                Entity enemy;
                if (enemy = hit.transform.GetComponent<Entity>())
                {
                    enemy.health -= damage;
                }
            }
        }
    }
}