﻿using UnityEngine;

namespace CarbonVanguard
{
    public class Grenade : Projectile
    {
        public float splashDamage;
        public float radius;

        private void GrenadeDamage(Entity directHit)
        {
            Collider[] entsInSplash = Physics.OverlapSphere(transform.position, radius);

            if (directHit != null)
                directHit.TakeDamage(damage);

            for (int i = 0; i < entsInSplash.Length; i++)
            {
                Entity e = entsInSplash[i].GetComponent<Entity>();

                //Deal damage to enemies in radius, make sure that the entity that takes direct hit damage doesn't also take splash damage
                if (e != null && e != directHit)
                {
                    e.TakeDamage(splashDamage);
                }
            }
        }

        protected override void OnTriggerEnter(Collider other)
        {
            Entity target = other.GetComponent<Entity>();
            GrenadeDamage(target);

            if (effectName != null)
            {
                ObjectPool.RequestObject(effectName, transform.position, transform.rotation);
            }

            Reset();
            ObjectPool.DeactivateObject(projectileName, gameObject);
        }
    }
}