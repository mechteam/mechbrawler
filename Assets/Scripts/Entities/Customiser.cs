﻿//#define DEBUG_CUSTOMISER
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace CarbonVanguard
{
    public class Customiser : MonoBehaviour
    {
        public Mech target;
        private MechPart selectedPart;

        public EquipmentEntry[] equipmentEntries;
        public Transform[] customisationVisualisers;

        public MechEquipment[] selectedEqipmentList;
        private Dictionary<PartType, MechEquipment[]> avaliableEquipment;
        private GameObject[] attachedParts;

        public event GenericEvent MechPartSelected;
        public event GenericEvent MechEquipmentSet;

        [Serializable]
        public struct EquipmentEntry
        {
            public PartType category;
            public MechEquipment[] equipment;
        }

        public void OnEnable()
        {
            CharInput.LockMouse(false);
        }

        public void OnDisable()
        {
            CharInput.LockMouse(true);
        }

        public void SelectPart(int index)
        {
            selectedPart = target.GetPart(index);

            if (selectedPart == null)
                selectedEqipmentList = null;
            else
                selectedEqipmentList = avaliableEquipment[selectedPart.partType];

            if(MechPartSelected != null)
                MechPartSelected();
        }

        public void SelectEquipment(int index)
        {
            if (selectedPart != null)
            {
                MechPart[] allParts = target.GetParts().ToArray();

                if (selectedPart.partType == PartType.Leg)
                {
                    for (int i = 0; i < allParts.Length; i++)
                    {
                        MechPart part = allParts[i];

                        if (part != selectedPart && part.partType == PartType.Leg)
                        {
                            target.Equip(part.equipment, selectedEqipmentList[index]);
                        }
                    }
                }
                else if (selectedPart.partType == PartType.Armour)
                {
                    MechArmour a = selectedEqipmentList[index] as MechArmour;

                    selectedPart.armour = a.armour;
                    target.AssignArmour(selectedPart);

                    if (attachedParts[selectedPart.attatchIndex] != null)
                        Destroy(attachedParts[selectedPart.attatchIndex]);

                    GameObject visequip = Instantiate<GameObject>(selectedEqipmentList[index].model, customisationVisualisers[selectedPart.attatchIndex]);
                    visequip.transform.localPosition = Vector3.zero;
                    visequip.transform.forward = customisationVisualisers[index].forward;

                    attachedParts[selectedPart.attatchIndex] = visequip;
                }
                else
                {
                    target.Equip(selectedPart.equipment, selectedEqipmentList[index]);

                    if (customisationVisualisers.Length > selectedPart.attatchIndex)
                    {
                        if (attachedParts[selectedPart.attatchIndex] != null)
                            Destroy(attachedParts[selectedPart.attatchIndex]);

                        GameObject visequip = Instantiate<GameObject>(selectedEqipmentList[index].model, customisationVisualisers[selectedPart.attatchIndex]);
                        visequip.transform.localPosition = Vector3.zero;
                        visequip.transform.forward = customisationVisualisers[index].forward;

                        attachedParts[selectedPart.attatchIndex] = visequip;
                    }
                }
            }

            if (MechEquipmentSet != null)
                MechEquipmentSet();
        }

        private void Start()
        {
            avaliableEquipment = new Dictionary<PartType, MechEquipment[]>();

            foreach (EquipmentEntry entry in equipmentEntries)
            {
                avaliableEquipment.Add(entry.category, entry.equipment);
            }

            attachedParts = new GameObject[4];
        }

#if DEBUG_CUSTOMISER
        private void OnGUI()
        {
            GUI.Label(new Rect(16, 16, 200, 25), string.Format("Selected Part: {0}", selectedPart != null ? selectedPart.uiName : "null"));
            GUI.Label(new Rect(16, 32, 400, 25), string.Format("Selected Equipment: {0}", selectedPart != null && selectedPart.equipment.data != null ? selectedPart.equipment.data.uiName : "null"));
        }
#endif
    }
}