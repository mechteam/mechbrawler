﻿using UnityEngine;
using UnityEngine.Events;

namespace CarbonVanguard
{
    public delegate void EntityEvent(Entity ent, params object[] data);

    [RequireComponent(typeof(Collider))]
    public abstract class Entity : MonoBehaviour, IResettable
    {
        public string uiName;
        public UnityEvent OnDeath;
        [HideInInspector] public UnityEvent OnHealthChange;
        public abstract void Reset();
        protected bool dead = false;

        public void Awake()
        {
            health.Current = health.Max;
        }

        public virtual void TakeDamage(float amount)
        {
            health.Current -= amount;
            if (health.Current <= 0f)
            {
                OnDeath.Invoke();
                OnDie();
            }
        }

        protected virtual void OnDie()
        {
            dead = true;
        }

        public Stat health;
    }
}