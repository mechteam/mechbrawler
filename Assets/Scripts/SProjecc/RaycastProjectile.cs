﻿using UnityEngine;

namespace SProjectile
{
    public class RaycastProjectile : ProjectileBase
    {
        private LineRenderer trail;

        private Vector3 lastPosition;
        private float speed;
        private Vector3 originForward;

        private void Update()
        {
            if (Physics.Raycast(lastPosition, originForward * speed * Time.deltaTime, out RaycastHit hit, speed, hitLayers))
            {
                OnHit(hit);
            }
            else
            {
                OnMiss();
            }
        }

        private void OnHit(RaycastHit hit)
        {
            IHittable hitObject = hit.transform.GetComponent<IHittable>();

            if (hitObject != null)
            {
                hitObject.OnHit(damage);

            }
            if (impactEffect)
            {
                Instantiate(impactEffectSystem, hit.point, Quaternion.Euler(hit.normal), null).transform.forward = hit.normal;
            }

            //Debug.Log("Hit");

            Deallocate();
        }

        private void OnMiss()
        {
            Debug.DrawLine(lastPosition, originForward + transform.forward * speed + lastPosition);

            

            if (useTrail)
                trail.SetPosition(0, lastPosition);
            lastPosition += originForward + transform.forward * speed * Time.deltaTime;


            if (useTrail)
                trail.SetPosition(1, lastPosition);
            //Debug.Log(lastPosition);
        }

        public override void Fire(float damage, float velocity, Vector3 direction)
        {
            hitLayers = ~0;

            originForward = transform.forward;

            lastPosition = transform.position;
            if (useTrail)
            {
                trail = GetComponent<LineRenderer>();
                if(trail == null)
                {
                    Debug.LogWarning("LineRenderer not present on the object. One has been created for now, but will need to be added in Edit Mode.", this);
                    trail = gameObject.AddComponent<LineRenderer>();
                }
                trail.SetPosition(0, lastPosition);
            }
            this.damage = damage;
            speed = velocity;
        }

        public override void Fire(float damage, float velocity, Vector3 direction, LayerMask layers)
        {
            hitLayers = layers;
        }

        public override void Fire(float damage, float velocity, Vector3 direction, LayerMask layers, ParticleSystem hitEffect)
        {
            hitLayers = layers;
        }

        public override void Fire(float damage, float velocity, Vector3 direction, ParticleSystem hitEffect)
        {
            impactEffectSystem = hitEffect;
            Fire(damage, velocity, direction);
        }

        protected override void Deallocate()
        {
            Destroy(gameObject);
        }
    }
}