﻿namespace SProjectile
{
    public interface IHittable
    {
        void OnHit(float damage);
    }
}