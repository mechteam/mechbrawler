﻿using UnityEngine;

public abstract class ProjectileBase : MonoBehaviour
{
    public delegate void ObjectReturn(in GameObject item);

    public bool haveTrail;
    public float damage;
    public LayerMask hitLayers = ~0;
    public bool impactEffect;
    public ParticleSystem impactEffectSystem;

    public event ObjectReturn Deallocation;

    public bool useTrail;

    public abstract void Fire(float damage, float velocity, Vector3 direction);
    public abstract void Fire(float damage, float velocity, Vector3 direction, LayerMask layers);
    public abstract void Fire(float damage, float velocity, Vector3 direction, LayerMask layers, ParticleSystem hitEffect);
    public abstract void Fire(float damage, float velocity, Vector3 direction, ParticleSystem hitEffect);

    protected virtual void Deallocate()
    {
        if (Deallocation != null)
            Deallocation.Invoke(gameObject);
        else
            Destroy(gameObject);
    }
}
