﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextHealth : MonoBehaviour
{

    public float updateInterval;
    public Part part;

    private Text text;


    void Start()
    {
        text = GetComponent<Text>();

        InvokeRepeating("UpdateText", 0, updateInterval);
    }

    void UpdateText()
    {
        text.text = part.GetCurrentHealth().ToString();
    }
}
