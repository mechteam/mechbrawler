﻿using UnityEngine;
using UnityEngine.Events;

public class Part : MonoBehaviour
{
    public float maxHealth;
    public UnityEvent onDie;

    private float currentHealth;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public float GetCurrentHealth()
    {
        return currentHealth;
    }

    public void AdjustHealth(float amount)
    {
        if (currentHealth <= 0f)
        {
            Die();
            return;
        }
            currentHealth += amount;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }

    void Die()
    {
        onDie.Invoke();
    }

}
