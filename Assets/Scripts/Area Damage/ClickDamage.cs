﻿using UnityEngine;

public class ClickDamage : MonoBehaviour
{
    public float modifyAmount = 10f;

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if(Physics.Raycast(ray, out hit))
            {
                Part part = hit.transform.GetComponent<Part>();
                part.AdjustHealth(-modifyAmount);
            }
        }
        else if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (Physics.Raycast(ray, out hit))
            {
                Part part = hit.transform.GetComponent<Part>();
                part.AdjustHealth(modifyAmount);
            }
        }
    }
}
