﻿using UnityEngine;

namespace CarbonVanguard
{
    public class RespawnEnemy : MonoBehaviour
    {
        public float respawnTime;

        public TrainingDummy[] dummies;

        private Utils.Timer[] timers;

        private void Start()
        {
            timers = new Utils.Timer[dummies.Length];
            for(int i = 0; i < timers.Length; i++)
            {
                timers[i] = new Utils.Timer(respawnTime, this);
                dummies[i].respawnIndex = i;
            }
            foreach(TrainingDummy dummy in dummies)
            {
                timers[dummy.respawnIndex].TimerElapsed += delegate { Respawn(dummy); };
            }
        }

        public void AddDeath(TrainingDummy item)
        {
            timers[item.respawnIndex].TimerOnce();
        }

        public void Respawn(TrainingDummy item)
        {
            item.gameObject.SetActive(true);
            item.Reset();
        }
    }
}