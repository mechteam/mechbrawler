﻿using UnityEngine;

namespace CarbonVanguard
{
    public class Spin : MonoBehaviour
    {
        public Vector3 desiredSpeed;
        [Range(0,1)]
        public float lerpSpeed;

        private Vector3 speed = Vector3.zero;
        private void Update()
        {
            //if (speed != Vector3.zero && desiredSpeed != Vector3.zero)
            //{
                speed = Vector3.Lerp(speed, desiredSpeed, lerpSpeed);
                transform.Rotate(speed);
            //}
        }
    }
}