﻿using UnityEngine;
using UnityEngine.Events;

namespace CarbonVanguard
{
    [RequireComponent(typeof(Collider))]
    public class Trigger : MonoBehaviour
    {
        public UnityEvent onTrigger;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
                onTrigger.Invoke();
        }
    }
}