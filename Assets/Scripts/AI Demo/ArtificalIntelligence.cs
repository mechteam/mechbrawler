﻿using UnityEngine;
using CarbonVanguard.Utils;

namespace CarbonVanguard.AI
{
    [RequireComponent(typeof(EquipmentSlot))]
    public abstract class ArtificalIntelligence : MonoBehaviour
    {
        [Tooltip("The delay between each check the AI does for the target")]
        public float sightCheckInterval = 1f;
        public float weaponCheckInterval = 0.5f;
        public GameObject target;
        public float sightRange = 20f;
        public float weaponRange = 10f;
        [Range(0, 180)]
        public float sightAngle = 90f;

        protected Timer sightTimer;
        protected Timer weaponTimer;

        protected EquipmentSlot weapon;
        protected float distanceToTarget = 0f;

        private void Awake()
        {
            if (target == null)
            {
                Debug.LogError("AI does not have a preset target. This AI Agent has been disabled.", this);
                enabled = false;
                return;
            }
            weapon = GetComponent<EquipmentSlot>();
            if(weapon == null)
            {
                Debug.LogError("No equipment slot detected. AI will not attack");
                enabled = false;
                return;
            }
            weapon.Startup();

            sightTimer = new Timer(sightCheckInterval, this);
            weaponTimer = new Timer(weaponCheckInterval, this);

            sightTimer.TimerElapsed += SightCheck;
            weaponTimer.TimerElapsed += WeaponCheck;

            sightTimer.TimerRepeating();
        }

        protected abstract void SightCheck();
        protected abstract void WeaponCheck();

        private void OnDisable()
        {
            DestroyAI();
        }

        private void OnDestroy()
        {
            DestroyAI();
        }

        public virtual void DestroyAI()
        {
            CancelInvoke("AICheck");
            weapon.StopTimer(ref weaponTimer);
        }
    }
}