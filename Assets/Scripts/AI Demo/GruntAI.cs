﻿using UnityEngine;
using UnityEngine.AI;

namespace CarbonVanguard.AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class GruntAI : ArtificalIntelligence
    {
        private NavMeshAgent agent;

        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            if (agent == null)
            {
                Debug.LogError("NavMeshAgent cannot be found. AI will not move", this);
                enabled = false;
                return;
            }
        }

        protected override void SightCheck()
        {
            if (Vector3.Angle(transform.forward, (target.transform.position - transform.position).normalized) <= sightAngle)
            {
                if (Physics.Raycast(transform.position, (target.transform.position - transform.position).normalized, out RaycastHit hit, sightRange))
                {
                    if (hit.transform.CompareTag("Player"))
                    {
                        if (!weaponTimer.isRunning)
                            weaponTimer.TimerRepeating();
                        distanceToTarget = hit.distance;
                        agent.SetDestination(hit.transform.position);
                    }
                    else
                        weaponTimer.TimerStop();
                }
                else
                    weaponTimer.TimerStop();
            }
            else
                weaponTimer.TimerStop();
        }

        protected override void WeaponCheck()
        {
            if (distanceToTarget >= weaponRange)
                return;

            weapon.Use();
        }
    }
}