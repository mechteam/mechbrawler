// GENERATED AUTOMATICALLY FROM 'Assets/Input/CVInput.inputactions'

using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace CarbonVanguard
{
    public class CVInput : IInputActionCollection
    {
        private InputActionAsset asset;
        public CVInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""CVInput"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""38c63099-7bb7-48d3-8bb6-c307c5e76bea"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""7803f7cc-d456-4334-8fc5-ed178feb9b64"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Button"",
                    ""id"": ""db66f24b-d77a-4baa-af6c-0c6b9a020949"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MobilityItem"",
                    ""type"": ""Button"",
                    ""id"": ""9f240f91-eca8-4d4d-a8f0-259545519855"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""2cb039b6-0e87-4a52-a6cf-f061e20cf89c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""18d8c3eb-0349-4a2a-9509-33340ae1c76e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""356789c8-9ece-4782-8991-a15ff2443fdc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""HeavyWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""4fed69d2-19cd-47d5-afca-c7a24e0dd2fd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ReloadLeft"",
                    ""type"": ""Button"",
                    ""id"": ""3e1699f7-05f3-4f21-bc89-25ed115bf7ec"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ReloadRight"",
                    ""type"": ""Button"",
                    ""id"": ""ae2c1221-1a8c-4a39-a61a-aeb3fbb239a5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ReloadBoth"",
                    ""type"": ""Button"",
                    ""id"": ""24e4fbdf-b642-40b4-9d83-9203e3317306"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleObjective"",
                    ""type"": ""Button"",
                    ""id"": ""ddb103ba-88e0-46a3-9b21-4e781f224236"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Zoom"",
                    ""type"": ""Button"",
                    ""id"": ""d44020e9-e17a-42a5-879a-dbe51ade24f5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Keys"",
                    ""id"": ""359b4bd5-9b08-4339-bb67-767cc5d499f3"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""85dd6168-953b-40c0-ac5a-c76604eae90c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""56c056d1-95b9-4576-8b14-2c9e37c9f7f5"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""937032cd-416e-4346-ad7e-f1a088c935ac"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""df879c39-7be4-453d-a086-e1ae70ba2905"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b1a88fb7-a769-4baf-a59b-1636b319c755"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""50d61f57-9306-4cd6-8246-195052044ade"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""MobilityItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""179f0854-a2b9-4b0b-bac8-1246b3665c34"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""12b9c627-2ee4-474b-8d2b-534ec923ac76"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""LeftWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df293970-beea-47a5-b933-73e141be1dd9"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""RightWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d163df9b-101a-443e-bf14-8ccbf352637b"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""HeavyWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef10dbfc-768e-4264-889b-95abd02ebe49"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ReloadLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7e43f5f0-48e0-4620-a3ce-7585a572ab11"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ReloadRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""15a5b83a-d7de-4d76-853f-c4b587053f87"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ReloadBoth"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97ae5be9-93dc-404f-b177-0a1fa71962d4"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""ToggleObjective"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dbdfc711-62b8-41cb-bfbc-794c26f5a6f9"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardMouse"",
                    ""action"": ""Zoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardMouse"",
            ""bindingGroup"": ""KeyboardMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Player
            m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
            m_Player_Movement = m_Player.FindAction("Movement", throwIfNotFound: true);
            m_Player_Look = m_Player.FindAction("Look", throwIfNotFound: true);
            m_Player_MobilityItem = m_Player.FindAction("MobilityItem", throwIfNotFound: true);
            m_Player_Pause = m_Player.FindAction("Pause", throwIfNotFound: true);
            m_Player_LeftWeapon = m_Player.FindAction("LeftWeapon", throwIfNotFound: true);
            m_Player_RightWeapon = m_Player.FindAction("RightWeapon", throwIfNotFound: true);
            m_Player_HeavyWeapon = m_Player.FindAction("HeavyWeapon", throwIfNotFound: true);
            m_Player_ReloadLeft = m_Player.FindAction("ReloadLeft", throwIfNotFound: true);
            m_Player_ReloadRight = m_Player.FindAction("ReloadRight", throwIfNotFound: true);
            m_Player_ReloadBoth = m_Player.FindAction("ReloadBoth", throwIfNotFound: true);
            m_Player_ToggleObjective = m_Player.FindAction("ToggleObjective", throwIfNotFound: true);
            m_Player_Zoom = m_Player.FindAction("Zoom", throwIfNotFound: true);
        }

        ~CVInput()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Player
        private readonly InputActionMap m_Player;
        private IPlayerActions m_PlayerActionsCallbackInterface;
        private readonly InputAction m_Player_Movement;
        private readonly InputAction m_Player_Look;
        private readonly InputAction m_Player_MobilityItem;
        private readonly InputAction m_Player_Pause;
        private readonly InputAction m_Player_LeftWeapon;
        private readonly InputAction m_Player_RightWeapon;
        private readonly InputAction m_Player_HeavyWeapon;
        private readonly InputAction m_Player_ReloadLeft;
        private readonly InputAction m_Player_ReloadRight;
        private readonly InputAction m_Player_ReloadBoth;
        private readonly InputAction m_Player_ToggleObjective;
        private readonly InputAction m_Player_Zoom;
        public struct PlayerActions
        {
            private CVInput m_Wrapper;
            public PlayerActions(CVInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Movement => m_Wrapper.m_Player_Movement;
            public InputAction @Look => m_Wrapper.m_Player_Look;
            public InputAction @MobilityItem => m_Wrapper.m_Player_MobilityItem;
            public InputAction @Pause => m_Wrapper.m_Player_Pause;
            public InputAction @LeftWeapon => m_Wrapper.m_Player_LeftWeapon;
            public InputAction @RightWeapon => m_Wrapper.m_Player_RightWeapon;
            public InputAction @HeavyWeapon => m_Wrapper.m_Player_HeavyWeapon;
            public InputAction @ReloadLeft => m_Wrapper.m_Player_ReloadLeft;
            public InputAction @ReloadRight => m_Wrapper.m_Player_ReloadRight;
            public InputAction @ReloadBoth => m_Wrapper.m_Player_ReloadBoth;
            public InputAction @ToggleObjective => m_Wrapper.m_Player_ToggleObjective;
            public InputAction @Zoom => m_Wrapper.m_Player_Zoom;
            public InputActionMap Get() { return m_Wrapper.m_Player; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerActions instance)
            {
                if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
                {
                    Movement.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                    Movement.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                    Movement.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                    Look.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                    Look.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                    Look.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                    MobilityItem.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMobilityItem;
                    MobilityItem.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMobilityItem;
                    MobilityItem.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMobilityItem;
                    Pause.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                    Pause.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                    Pause.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPause;
                    LeftWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftWeapon;
                    LeftWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftWeapon;
                    LeftWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftWeapon;
                    RightWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightWeapon;
                    RightWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightWeapon;
                    RightWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightWeapon;
                    HeavyWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyWeapon;
                    HeavyWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyWeapon;
                    HeavyWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyWeapon;
                    ReloadLeft.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadLeft;
                    ReloadLeft.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadLeft;
                    ReloadLeft.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadLeft;
                    ReloadRight.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadRight;
                    ReloadRight.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadRight;
                    ReloadRight.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadRight;
                    ReloadBoth.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadBoth;
                    ReloadBoth.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadBoth;
                    ReloadBoth.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnReloadBoth;
                    ToggleObjective.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleObjective;
                    ToggleObjective.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleObjective;
                    ToggleObjective.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleObjective;
                    Zoom.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnZoom;
                    Zoom.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnZoom;
                    Zoom.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnZoom;
                }
                m_Wrapper.m_PlayerActionsCallbackInterface = instance;
                if (instance != null)
                {
                    Movement.started += instance.OnMovement;
                    Movement.performed += instance.OnMovement;
                    Movement.canceled += instance.OnMovement;
                    Look.started += instance.OnLook;
                    Look.performed += instance.OnLook;
                    Look.canceled += instance.OnLook;
                    MobilityItem.started += instance.OnMobilityItem;
                    MobilityItem.performed += instance.OnMobilityItem;
                    MobilityItem.canceled += instance.OnMobilityItem;
                    Pause.started += instance.OnPause;
                    Pause.performed += instance.OnPause;
                    Pause.canceled += instance.OnPause;
                    LeftWeapon.started += instance.OnLeftWeapon;
                    LeftWeapon.performed += instance.OnLeftWeapon;
                    LeftWeapon.canceled += instance.OnLeftWeapon;
                    RightWeapon.started += instance.OnRightWeapon;
                    RightWeapon.performed += instance.OnRightWeapon;
                    RightWeapon.canceled += instance.OnRightWeapon;
                    HeavyWeapon.started += instance.OnHeavyWeapon;
                    HeavyWeapon.performed += instance.OnHeavyWeapon;
                    HeavyWeapon.canceled += instance.OnHeavyWeapon;
                    ReloadLeft.started += instance.OnReloadLeft;
                    ReloadLeft.performed += instance.OnReloadLeft;
                    ReloadLeft.canceled += instance.OnReloadLeft;
                    ReloadRight.started += instance.OnReloadRight;
                    ReloadRight.performed += instance.OnReloadRight;
                    ReloadRight.canceled += instance.OnReloadRight;
                    ReloadBoth.started += instance.OnReloadBoth;
                    ReloadBoth.performed += instance.OnReloadBoth;
                    ReloadBoth.canceled += instance.OnReloadBoth;
                    ToggleObjective.started += instance.OnToggleObjective;
                    ToggleObjective.performed += instance.OnToggleObjective;
                    ToggleObjective.canceled += instance.OnToggleObjective;
                    Zoom.started += instance.OnZoom;
                    Zoom.performed += instance.OnZoom;
                    Zoom.canceled += instance.OnZoom;
                }
            }
        }
        public PlayerActions @Player => new PlayerActions(this);
        private int m_KeyboardMouseSchemeIndex = -1;
        public InputControlScheme KeyboardMouseScheme
        {
            get
            {
                if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardMouse");
                return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
            }
        }
        public interface IPlayerActions
        {
            void OnMovement(InputAction.CallbackContext context);
            void OnLook(InputAction.CallbackContext context);
            void OnMobilityItem(InputAction.CallbackContext context);
            void OnPause(InputAction.CallbackContext context);
            void OnLeftWeapon(InputAction.CallbackContext context);
            void OnRightWeapon(InputAction.CallbackContext context);
            void OnHeavyWeapon(InputAction.CallbackContext context);
            void OnReloadLeft(InputAction.CallbackContext context);
            void OnReloadRight(InputAction.CallbackContext context);
            void OnReloadBoth(InputAction.CallbackContext context);
            void OnToggleObjective(InputAction.CallbackContext context);
            void OnZoom(InputAction.CallbackContext context);
        }
    }
}
